/*
  Message app settings related value and utilities.
*/

/* global MobileOperator, Promise */

/* exported Settings */


'use strict';

var Settings = {
  SERVICE_ID_KEYS: {
    smsServiceId: 'ril.sms.defaultServiceId',
    telephonyServiceId: 'ril.telephony.defaultServiceId',
    dataServiceId: 'ril.data.defaultServiceId',
    dataIccId: 'ril.data.defaultServiceId.iccId',
    backUpServiceId: 'ril.backUp.data.defaultServiceId',
    backUpIccId: 'ril.backUp.data.defaultServiceId.iccId'
  },

  READ_AHEAD_THREADS_KEY: 'ril.sms.maxReadAheadEntries',

  // we evaluate to 5KB the size overhead of wrapping a payload in a MMS
  MMS_SIZE_OVERHEAD: 5 * 1024,

  _serviceIds: null,

  // we need to remove this when email functionality is ready.
  supportEmailRecipient: true,

  // We set the default maximum concatenated number of our SMS app to 10
  // based on:
  // https://bugzilla.mozilla.org/show_bug.cgi?id=813686#c0
  maxConcatenatedMessages: 10,
  mmsSizeLimitation: 295 * 1024, // Default mms message size limitation is 295K
  smsServiceId: null, // Default sms service SIM ID
  telephonyServiceId: null,
  dataServiceId: null, // Default data service SIM ID
  dataIccId: null, // Default data service ICC ID
  backUpServiceId: null, // DSDS mms switch data connection back up.
  backUpIccId: null,
  emailAppInstalled: false,
  mmsEnable: true,
  // Bug876-gang-chen@t2mobile.com-FR DSDS sim card name begin
  sim1Name: null,
  sim2Name: null,
  // Bug876-gang-chen@t2mobile.com-FR DSDS sim card name end

  init: function settings_init() {
    var keyHandlerSet = {
      'dom.mms.operatorSizeLimitation': this.initMmsSizeLimitation.bind(this),
      'operatorResource.sms.maxConcat':
        this.initSmsMaxConcatenatedMsg.bind(this)
    };
    var settings = navigator.mozSettings;
    var conns = navigator.mozMobileConnections;

    function setHandlerMap(key) {
      var req = settings.createLock().get(key);
      req.onsuccess = function settings_getSizeSuccess() {
        var handler = keyHandlerSet[key];
        handler(req.result[key]);
      };
    }

    this._serviceIds = [];

    //Bug2645-gang-chen@t2mobile.com-FR DSDS sim card name update-begin
    // Only DSDS will need to handle sim card name
    if (conns && conns.length > 1) {
        for (var i = 0, l = conns.length; i < l; i++) {
         // Defect4327-tingting.chao@t2mobile.com-begin
         // this.updateSIMCardName(i);
          this.updateSIMCardName(i, function(cardIndex, cardName) {
            if (cardIndex === 0) {
              Settings.sim1Name = cardName;
            } else if (cardIndex === 1) {
              Settings.sim2Name = cardName;
            }
          });
          // Defect4327-tingting.chao@t2mobile.com-end
        }
    }
    //Bug2645-gang-chen@t2mobile.com-FR DSDS sim card name update-end

    if (!settings) {
      return;
    }

    settings.createLock().get('mms.enable').then(function(result) {
      if (result['mms.enable'] !== undefined) {
        Settings.mmsEnable = result['mms.enable'];
      }
      if (!Settings.mmsEnable) {
        Settings.supportEmailRecipient = false;
      }
      SettingsUI.init();
    });

    // Only DSDS will need to handle mmsServiceId
    if (conns && conns.length > 1) {
      for (var prop in this.SERVICE_ID_KEYS) {
        var setting = this.SERVICE_ID_KEYS[prop];
        keyHandlerSet[setting] = this.initServiceId.bind(this, setting, prop);
      }

      // Cache all existing serviceIds
      for (var i = 0, l = conns.length; i < l; i++) {
        this._serviceIds.push(conns[i].iccId);
      }
    }

    for (var key in keyHandlerSet) {
      setHandlerMap(key);
    }

    this.checkEmailInstalled();
  },

  //Set Maximum concatenated number of our SMS
  initSmsMaxConcatenatedMsg: function initSmsMaxConcatenatedMsg(num) {
    if (num && !isNaN(num)) {
      this.maxConcatenatedMessages = num;
    }
  },

  // Bug876-gang-chen@t2mobile.com-FR DSDS sim card name for Settings change-begin
  // read renamed SIM Card name
  getIccCardIMSI: function getIccCardIMSI(cardIndex) {
    var mobileConnections = navigator.mozMobileConnections;
    var iccCard = navigator.mozIccManager.getIccById(mobileConnections[cardIndex].iccId);
    return (iccCard && iccCard.iccInfo) ? iccCard.iccInfo.imsi : undefined;
  },

  // Defect4327-tingting.chao@t2mobile.com-begin
  //updateSIMCardName: function updateSIMCardName(cardIndex,callback) {
  updateSIMCardName: function updateSIMCardName(cardIndex,callback) {
  // Defect4327-tingting.chao@t2mobile.com-end
      var SETTING_KEY_CARD_NAME = 'card.customized_name';
      var imsi = this.getIccCardIMSI(cardIndex);
      var request = window.navigator.mozSettings.createLock().get(SETTING_KEY_CARD_NAME);
      request.onsuccess = function () {
        var fetched = false;
        var cardNameSettings = request.result[SETTING_KEY_CARD_NAME];
        if (cardNameSettings && cardNameSettings.length !== 0) {
          for (var index in cardNameSettings) {
            if (cardNameSettings[index].imsi && cardNameSettings[index].imsi === imsi) {
              // Defect4327-tingting.chao@t2mobile.com-begin
              /*if (index === 0) {
                Settings.sim1Name = cardNameSettings[index].cardName;
              } else {
                Settings.sim2Name = cardNameSettings[index].cardName;
              }*/
              callback(cardIndex, cardNameSettings[index].cardName);
              // Defect4327-tingting.chao@t2mobile.com-end
              fetched = true;
              break;
            }
          }
        }

        if (!fetched) {
          console.log('get all sim card name error in success');
          callback(cardIndex, 'SIM' + (cardIndex + 1));// Defect4327-tingting.chao@t2mobile.com-add
        }
      };
      request.onerror = function () {
        console.log('get all sim card name error: ' + request.error.name);
        callback(cardIndex, 'SIM' + (cardIndex + 1));// Defect4327-tingting.chao@t2mobile.com-add
      };
  },
  // Bug876-gang-chen@t2mobile.com-FR DSDS sim card name for Settings change-end

  // Set MMS size limitation:
  // If operator does not specify MMS message size, we leave the decision to
  // MessageManager and return nothing if we can't get size limitation from db
  initMmsSizeLimitation: function initMmsSizeLimitation(size) {

      // Task5066690-jipeng.sun@tcl.com-begin
      navigator.customization.getValue("messaging.mms.max.size").then(function (result) {
          /*if (JSON.stringify(result) !== undefined) {
              var resultSize = JSON.stringify(result);
              if (resultSize > 0) {
                  this.mmsSizeLimitation = resultSize - this.MMS_SIZE_OVERHEAD;*/
          dump("messaging.mms.max.size:" + result+",size :"+size);
          if (result !== undefined) {
              // customized for Argon-begin
              var mmsCustomizeSize = 0;
              switch (result) {
                  case 0:
                      mmsCustomizeSize = 100;
                      break;
                  case 1:
                      mmsCustomizeSize = 300;
                      break;
                  case 2:
                      mmsCustomizeSize = 600;
                      break;
                  case 3:
                      mmsCustomizeSize = 1024;
                      break;
                  default:
                      mmsCustomizeSize = 300;
                      break;
              }
              // customized for Argon-end

              if (result >= 0) { //Bug1430-gang-chen@t2mobile.com-max size customization
                  Settings.mmsSizeLimitation = mmsCustomizeSize * 1024- Settings.MMS_SIZE_OVERHEAD;
                  console.log("Settings.mmsSizeLimitation:" + Settings.mmsSizeLimitation);
              } else {
                  if (size && !isNaN(size)) {
                      Settings.mmsSizeLimitation = size - Settings.MMS_SIZE_OVERHEAD;
                  }
              }
              console.log("messaging.mms.max.size,get customization value:" + result + ",mmsSizeLimitation:" + Settings.mmsSizeLimitation);
          } else {
              console.log("messaging.mms.max.size is undefined,use default size:" + size);
              if (size && !isNaN(size)) {
                  Settings.mmsSizeLimitation = size - Settings.MMS_SIZE_OVERHEAD;
              }
          }
      });
      // Task5066690-jipeng.sun@tcl.com-end
  },

  // Set default mms service SIM ID and add observer:
  // In DSDS scenario, if we notify user to switch to subscription to retrieve
  // the MMS from non-active subscription, we'll need current mmsServiceId
  // information to tell user the active/non-active subscription
  initServiceId: function initMmsServiceId(settingName, propName, id) {
    if (id !== undefined) {
      Settings[propName] = id;
    }
    navigator.mozSettings.addObserver(settingName, function(e) {
      Settings[propName] = e.settingValue;
    });
  },

  setMmsSimServiceId: function setSimServiceId(id, iccId) {
    // DSDS: mms & data are both necessary for connection switch.
    // Need set IccID to confirm the data can not be reset to
    // default by ststem protection rule.
    navigator.mozSettings.createLock().set({
      'ril.data.defaultServiceId': id,
      'ril.data.defaultServiceId.iccId': iccId
    });
  },

  switchMmsSimHandler: function switchSimHandler(targetId, iccId) {
    var conn = window.navigator.mozMobileConnections[targetId];
    return new Promise(function(resolve, reject) {
      if (conn) {
        if (conn.data.connected) {
          // Call resolve directly if data connected already
          resolve();
        } else {
          // Listen to MobileConnections datachange to make sure we can start
          // to retrieve mms only when data.connected is true. But we can't
          // guarantee datachange event will work in other device.
          conn.addEventListener('datachange', function onDataChange() {
            if (conn.data.state === 'registered') {
              conn.removeEventListener('datachange', onDataChange);
              resolve();
            }
          });


          this.setMmsSimServiceId(targetId, iccId);
        }
      } else {
        reject('Invalid connection');
      }
    }.bind(this));
  },

  /**
   * returns true if the device has more than 1 SIM port
   */
  isDualSimDevice: function isDualSimDevice() {
    return this._serviceIds && this._serviceIds.length > 1;
  },

  /**
   * Returns true if the device has more than 1 SIM port and at least 2 SIMs are
   * inserted.
   */
  hasSeveralSim: function hasSeveralSim() {
    if (!this.isDualSimDevice()) {
      return false;
    }

    var simCount = this._serviceIds.reduce(function(simCount, iccId) {
      return iccId === null ? simCount : simCount + 1;
    }, 0);

    return simCount > 1;
  },

  getServiceIdByIccId: function getServiceIdByIccId(iccId) {
    if (!this._serviceIds) {
      return null;
    }

    var index = this._serviceIds.indexOf(iccId);

    return index > -1 ? index : null;
  },

  /**
   * Will return SIM1 or SIM2 (locale dependent) depending on the iccId.
   * Will return the empty string in a single SIM scenario.
   */
  getSimNameByIccId: function getSimNameByIccId(iccId) {
    var index = this.getServiceIdByIccId(iccId);
    if (index === null) {
      return '';
    }

    var simName = navigator.mozL10n.get('sim-id-label', { id: index + 1 });
    return simName;
  },

  /**
   * Will return operator name depending on the iccId.
   * Will return the empty string in a single SIM scenario.
   */
  getOperatorByIccId: function getOperatorByIccId(iccId) {
    var index = this.getServiceIdByIccId(iccId);
    if (index === null) {
      return '';
    }

    var conn = navigator.mozMobileConnections[index];
    return MobileOperator.userFacingInfo(conn).operator;
  },

  setReadAheadThreadRetrieval: function(value) {
    if (!navigator.mozSettings) {
      return;
    }

    var setting = {};
    setting[this.READ_AHEAD_THREADS_KEY] = value;
    navigator.mozSettings.createLock().set(setting);
  },

  checkEmailInstalled: function() {
    var request = window.navigator.mozApps.mgmt.getAll();
    request.onerror = function(e) {
      console.log('get all installed apps error: ' + request.error.name);
    };
    request.onsuccess = function(evt) {
      var installedApps = evt.target.result;
      Settings.emailAppInstalled = installedApps.some(function(app) {
        return (app.manifestURL === 'app://email.gaiamobile.org/manifest.webapp');
      });
    };
  }
};


/*
  define export SettingsUI
 */

(function(exports) {

  'use strict';

  var skSettingsOK = {
    l10nId: 'select',
    priority: 2,
    method: function() {
    }
  };

  var SettingsUI = {
    initialized: false,

    init: function sui_init() {
      var settings = document.getElementById('messaging-settings');
      if (settings.innerHTML.length === 0) {
        var template;
        if (Settings.mmsEnable) {
          template = Template('messaging-settings-view-tmpl');
        } else {
          template = Template('messaging-settings-view-tmpl-noMMS');
        }
        settings.innerHTML = template.toString();
      }
      // bug1825-chengyanzhang@t2moblie.com-for disable emergency alert-begin
      navigator.customization.getValue('def.enable.cellbroadcast').then((isEnableCellBroadcast) => {
        console.log('isEnableCellBroadcast===>' +isEnableCellBroadcast);
        if (isEnableCellBroadcast!== undefined && !isEnableCellBroadcast) {
          var emergencyAlert = document.getElementById('emergency-alert-item');
          var emergencyAlertHeader = document.getElementById('emergency-alert-header');
          if (emergencyAlert != undefined && emergencyAlert != null &&
            emergencyAlertHeader != undefined && emergencyAlertHeader !=null) {
            emergencyAlert.classList.add('hidden');
            emergencyAlert.classList.remove('navigable');
            emergencyAlertHeader.classList.add('hidden');
            emergencyAlertHeader.classList.remove('navigable');
          }
        }
      });
      // bug1825-chengyanzhang@t2moblie.com-for disable emergency alert-end
      navigator.mozSettings.addObserver('ril.sms.requestStatusReport.enabled',
        function(e) {
        var value = e.settingValue;
        navigator.mozSettings.createLock().set(
          {'ril.mms.requestStatusReport.enabled' : value});
      });

      this.selectWappushDisplay();
    },

    _updateSKs: function() {
      var params = {
        header: {l10nId: 'options'},
        items: [skSettingsOK]
      };
      if (exports.option) {
        exports.option.initSoftKeyPanel(params);
      } else {
        exports.option = new SoftkeyPanel(params);
      }
      exports.option.show();
    },

    beforeLeave: function() {
      window.removeEventListener('keydown', SettingsUI._handleKeyEvent);
    },

    afterEnter: function() {
      this._updateSKs();
      SettingsUI.render();
      window.addEventListener('keydown', SettingsUI._handleKeyEvent);
    },

    afterLeave: function(args) {
      if (exports.option) {
        exports.option.show();
      }
    },

    _handleKeyEvent: function(event) {
      switch (event.key) {
        case 'BrowserBack':
        case 'Backspace':
          event.preventDefault();
          SettingsUI._back();
          break;
        case 'Accept':
        case 'Enter':
          SettingsUI._openSelectItem();
          break;
        case 'ArrowDown':
          break;
        case 'ArrowUp':
          break;
      }
    },

    _back: function() {
      Navigation.toPanel('thread-list');
    },

    _openSelectItem: function(event) {
      var focusedItems = document.querySelectorAll('.focus');
      if (focusedItems.length > 0) {
        var focusItem = focusedItems[0];
        var viewPanel = SettingsUI._getViewPanel(focusItem);

        if (focusItem.classList.contains('disable-item')) {
          return;
        }

        if (focusItem.id === 'emergency-alert-item') {
          return new MozActivity({
              name: 'configure',
              data: { section: 'emergency-alert' }
          })
        }
        Navigation.toPanel(viewPanel);
      }
    },

    _getViewPanel: function(target) {
      return target.getAttribute('name');
    },

    selectWappushDisplay: function () {
      var isDualSim = Settings.isDualSimDevice();
      var viewSim1 = document.getElementsByName('wap-push-view-sim1')[0];
      var viewSim2 = document.getElementsByName('wap-push-view-sim2')[0];
      var viewSim1Span = viewSim1.getElementsByTagName('span')[0];
      var viewSim2Span = viewSim2.getElementsByTagName('span')[0];
      if (isDualSim) {
        viewSim1Span.setAttribute('data-l10n-id', 'wappushSIM1-ds');
        viewSim2Span.setAttribute('data-l10n-id', 'wappushSIM2-ds');
        // Bug876-gang-chen@t2mobile.com-FR DSDS sim card name begin
        viewSim1Span.setAttribute('data-l10n-args', JSON.stringify({ simname: Settings.sim1Name ? Settings.sim1Name : 'SIM 1' }));
        viewSim2Span.setAttribute('data-l10n-args', JSON.stringify({ simname: Settings.sim2Name ? Settings.sim2Name : 'SIM 2' }));
        // Bug876-gang-chen@t2mobile.com-FR DSDS sim card name end
      } else {
        viewSim1Span.setAttribute('data-l10n-id', 'wappushNormal');
        viewSim2.classList.add('hide');
        viewSim2.classList.remove('navigable');
      }
    },

    countNotAssignedSIM: function() {
      var count = 0;
      for (var i = 0; i < Settings._serviceIds.length; i++) {
        if (Settings._serviceIds[i] === null) {
          count++;
        }
      }
      return count;
    },

    render: function() {
      // Task5243959-tingtingchao@t2mobile.com-add-ril.sms.encoding_mode
      ['ril.sms.requestStatusReport.enabled','ril.mms.retrieval_mode', 'wap.push.enabled.sim1', 'wap.push.enabled.sim2','ril.sms.encoding_mode'].forEach(function(item) {
      // Task5318674-tingtingchao@t2mobile.com-add-ril.sms.encoding_mode-begin
        var request = window.navigator.mozSettings.createLock().get(item);
        request.onsuccess = function onSuccessCb() {
            var value = request.result[item];
            console.log("value is : " + value);
            var elem = document.getElementsByName(item)[0];
            console.log("elem is : " + value);
            var DualSIMSum = 2;
          switch (item) {
            case 'ril.mms.retrieval_mode':
              // Task5071877-jipeng.sun@tcl.com-customized for Argon-begin
              /*if (value === 'manual') {
                navigator.mozL10n.setAttributes(elem,
                  'switch-off');
              } else if (value === 'automatic') {
                navigator.mozL10n.setAttributes(elem,
                  'auto-roaming-retrieve');
              } else {
                navigator.mozL10n.setAttributes(elem,
                  'tcl-auto-no-roaming-retrieve');
              }*/
              // Defect1619-ting-wang@t2mobile.com-update default value-begin
              if (value == '1') {
                navigator.mozL10n.setAttributes(elem, 'tcl-auto-manual');
              } else if (value == '0') {
                  navigator.mozL10n.setAttributes(elem, 'auto-no-roaming-retrieve');
              }
               // Defect1619-ting-wang@t2mobile.com-update default value-end
                // Task5071877-jipeng.sun@tcl.com-customized for Argon-end
              break;
              // Task5243959-tingtingchao@t2mobile.com-begin
            case 'ril.sms.encoding_mode':
              if(value == 0){
                  navigator.mozL10n.setAttributes(elem, 'tcl-sms-reduced-foramt');
              }else if(value == 1) {
                  navigator.mozL10n.setAttributes(elem, 'tcl-sms-full-foramt');
              }
              break;
              // Task5243959-tingtingchao@t2mobile.com-end
            case 'ril.sms.requestStatusReport.enabled':
            case 'wap.push.enabled.sim1':
            case 'wap.push.enabled.sim2':
              var v = value === true ? 'switch-on' :'switch-off';
              navigator.mozL10n.setAttributes(elem, v);
              if (Settings.isDualSimDevice() && !Settings.hasSeveralSim()) {
                var notAssignedSIMId = Settings._serviceIds.indexOf(null);
                var notAssignedSIMCount = SettingsUI.countNotAssignedSIM();
                if ((notAssignedSIMCount === DualSIMSum ||
                     notAssignedSIMId === 0) &&
                    (item === 'wap.push.enabled.sim1')) {
                  navigator.mozL10n.setAttributes(elem, 'not-assigned');
                  document.getElementById('wapPush-message-item-sim1').
                    classList.add('disable-item');
                } else if ((notAssignedSIMCount === DualSIMSum ||
                            notAssignedSIMId === 1) &&
                           (item === 'wap.push.enabled.sim2')) {
                  navigator.mozL10n.setAttributes(elem, 'not-assigned');
                  document.getElementById('wapPush-message-item-sim2').
                    classList.add('disable-item');
                }
              }
            }
        }

    /*      navigator.customization.getValue(item).then(function (result) {
              var elem = document.getElementsByName(item)[0];
              //var value = JSON.stringify(result);
              console.log("result is : " + result);
              if (item == 'ril.mms.retrieval_mode') {
                  if (result == undefined) {
                    result = 'manual';
                   }
                  console.log("On retrieve item the result is: " + result);
                   if (result == 'manual') {
                     navigator.mozL10n.setAttributes(elem, 'tcl-switch-off');
                   } else if (result == 'automatic') {
                     navigator.mozL10n.setAttributes(elem, 'tcl-auto-roaming-retrieve');
                   } else {
                     navigator.mozL10n.setAttributes(elem, 'tcl-auto-no-roaming-retrieve');
                   }

                   // Task5243959-tingtingchao@t2mobile.com-begin
              } else if (item == 'ril.sms.encoding_mode'){
                  if(result == undefined){
                    result = 'reduced';
                  }
                  console.log("On encoding item the result is: " + result);
                   if (result == 'reduced'){
                     navigator.mozL10n.setAttributes(elem, 'tcl-sms-reduced-foramt');
                   } else {
                     navigator.mozL10n.setAttributes(elem, 'tcl-sms-full-foramt');
                   }
                  // Task5243959-tingtingchao@t2mobile.com-end
               } else {
                if (result == undefined) {
                    result = true;
                }
                console.log("On other item the result is: " + result);
                var v = result == true ? 'tcl-switch-on' :'tcl-switch-off';
                navigator.mozL10n.setAttributes(elem, v);
                }
          });*/
          // Task5318674-tingtingchao@t2mobile.com-add-ril.sms.encoding_mode-begin-end
      });
    }
  };

  exports.SettingsUI = SettingsUI;
}(this));
