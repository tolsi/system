/* © 2017 KAI OS TECHNOLOGIES (HONG KONG) LIMITED All rights reserved.
 * This file or any portion thereof may not be reproduced or used in any manner
 * whatsoever without the express written permission of KAI OS TECHNOLOGIES
 * (HONG KONG) LIMITED. KaiOS is the trademark of KAI OS TECHNOLOGIES (HONG KONG)
 * LIMITED or its affiliate company and may be registered in some jurisdictions.
 * All other trademarks are the property of their respective owners.
 */
// ************************************************************************
// * File Name: nfc.js
// * Description: mmitest -> test item: nfc test.
// * Note: check nfc
// ************************************************************************

/* global DEBUG, dump, TestItem */
'use strict';

function debug(s) {
  if (DEBUG) {
    dump('<mmitest> ------: [nfc.js] = ' + s + '\n');
  }
}

var NfcTest = new TestItem();
var nfcStatusItem = document.getElementById('centertext');
var startButton = document.getElementById('retestButton');

NfcTest.setItemString = function(content) {
  nfcStatusItem.innerHTML = content;
}

/**
 * Use the deviceStorage API to create a new file and save the CPLC data
 */
NfcTest.createFile = function(cplcResult) {
  let sdcard = NfcTest.sdcard;
  // We should save the CPLC data into internal storage area.
  sdcard.available().then((result) => {
    switch (result) {
      case 'available':
        sdcard.freeSpace().then((result) => {
          // We need to keep enough free space to save the CPLC data.
          let text = new Blob([cplcResult], { type: 'text/plain' });
          if (result && result > text.size) {
            sdcard.addNamed(text, NfcTest.fileName).then((result) => {
              this.setItemString('NFC testing passed and CPLC data saved.');
              debug('Success to save the CPLC data into /data/usbmsc_mnt/cplc.txt');
            }, (error) => {
              this.setItemString('NFC testing passed but CPLC data don\'t saved.');
              debug('Unable to save the CPLC data error: ' + error.name);
            });
          } else {
            this.setItemString('NFC testing passed but CPLC data don\'t saved.');
            debug('There is not enough free space to save the CPLC data.');
          }
        }, (err) => {
          this.setItemString('NFC testing passed but CPLC data don\'t saved.');
          debug('Can\'t get the internal sdcard free space: ' + err.name);
        });
        break;
      case 'unavailable':
        debug('Your device\'s SD Card is not available.');
        break;
      default:
        debug('Your device\'s SD Card is shared and thus not available.');
        break;
    }
  }, (error) => {
    debug('Unable to get the space used by the SD Card: ' + error.name);
  });
}

/**
 * Start NFC test with command
 */
NfcTest.startNfcTest = function() {
  navigator.engmodeExtension.execCmdLE(['nfc_test'], 1).then((result) => {
    debug('NfcTest execute command success and start to read nfc test data.');
    NfcTest.interval = setInterval(NfcTest.readNfcInfo.bind(this), 3000);
    NfcTest.sdcard.get(NfcTest.fileName).then(() => {
      NfcTest.sdcard.delete(NfcTest.fileName);
    });
  }).catch((error) => {
    debug('NfcTest execute command error: ' + error.name);
    this.setItemString('Execute NFC testing command failed.');
  });

  // We need to make the phone close to the NFC Reader between 3s and 8s.
  setTimeout(() => {
    this.setItemString('Make the phone close to the NFC Reader device.');
  }, 3500)
};

NfcTest.stopNfcTest = function() {
  return navigator.engmodeExtension.execCmdLE(['nfc_stop'], 1);
}

/**
 *  Save the CPLC data from out nfc test result.
 */
NfcTest.saveCplcData = function(arr) {
  let i = 0,
    len = arr.length,
    result = 'CPLC data:';
  for (i; i < len; i++) {
    let str = arr[i].replace(/^\s+/, '');
    let iccData =
      str.match(/^(ICC|IC|Operating)[a-zA-Z.:\D]*[\d+a-zA-Z()\s+]*/);
    if (iccData) {
      result = result + '\n' + iccData[0];
    }
  }
  this.createFile(result);
}

/**
 *  Read NFC testing result and display the result
 */
NfcTest.readNfcInfo = function() {
  var nfcString = navigator.engmodeExtension.fileReadLE('nfcInfo');
  if (!nfcString || nfcString === '') {
    debug('NfcTest can\'t get file content.');
    return;
  }

  // Because the debug function has the string size limit,
  //   we should change this way to console and print the full logs.
  console.log('NFC test result:\n' + nfcString);
  clearInterval(NfcTest.interval);

  // XXX: It's a workaround to display cureent NFC testing result
  if (nfcString.indexOf('Error') >= 0) {
    debug('NfcTest testing failed reason: device don\'t support NFC.');
    this.setItemString('Device don\'t support NFC');
  }

  if (nfcString.indexOf('Unknown') >= 0) {
    debug('NfcTest testing failed reason: Unknown');
    this.setItemString('NFC testing failed.');
  }

  if (nfcString.indexOf('FAILED') >= 0) {
    debug('NfcTest testing failed.');
    this.setItemString('NFC testing failed.');
  }

  if (nfcString.indexOf('PASSED') >= 0) {
    debug('NfcTest testing passed.');
    this.autoPass(1000);
    this.passButton.disabled = '';
    this.setItemString('NFC testing passed.');
    // Save CPLC data into "/data/usbmsc_mnt/cplc.txt"
    if (nfcString.indexOf('CPLC') >= 0) {
      let arr = nfcString.split('\n');
      this.setItemString('Saving CPLC data ....');
      this.saveCplcData(arr);
    } else {
      debug('There is no any CPLC data in the /data/nfc/test_result file.');
      this.setItemString('NFC testing passed but get CPLC data failed.')
    }
  }
};

NfcTest.onInit = function() {
  // Disable the PASS button first
  this.passButton.disabled = 'disabled';
  this.failButton.disabled = '';

  if (!navigator.engmodeExtension) {
    nfcStatusItem.innerHTML = 'KaiOS extension NOT support.';
    startButton.disabled = 'disabled';
    return;
  }

  let stop = this.stopNfcTest();
  stop.onsuccess = function() {
    // Create and elevate data file permissions
    navigator.engmodeExtension.execCmdLE(['data_nfc_upper'], 1);
  }

  stop.onerror = function() {
    // Create and elevate data file permissions
    navigator.engmodeExtension.execCmdLE(['data_nfc_upper'], 1);
  }

  // CPLC data file name
  this.fileName = 'cplc.txt';

  // Get the "/data/usbmsc_mnt/" storage area
  this.sdcard = navigator.getDeviceStorages('sdcard')[0];
};

NfcTest.onDeinit = function() {
  this.stopNfcTest();
  // Clear interval when exiting this page
  if (NfcTest.interval) {
    clearInterval(NfcTest.interval);
  }
};

NfcTest.onHandleEvent = function(evt) {
  switch (evt.key) {
    case 'ArrowUp':
      if (!startButton.hidden) {
        startButton.hidden = true;
        this.setItemString('Waiting for NFC test initing ...');
        setTimeout(this.startNfcTest.bind(this), 500)
      }
      evt.preventDefault();
      break;
  }
  return false;
};

window.addEventListener('load', NfcTest.init.bind(NfcTest));
window.addEventListener('beforeunload', NfcTest.uninit.bind(NfcTest));
window.addEventListener('keydown', NfcTest.handleKeydown.bind(NfcTest));
