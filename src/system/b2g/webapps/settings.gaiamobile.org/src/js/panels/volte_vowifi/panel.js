 /* global DsdsSettings */
 /* global SettingsSoftkey */
define(['require','shared/toaster','modules/settings_panel','modules/settings_service'],function(require) {
  
  var Toaster = require('shared/toaster');
  var SettingsPanel = require('modules/settings_panel');
  var SettingsService = require('modules/settings_service');

  return function volte_settings_panel() {
    var elements = {};
    var _settings = navigator.mozSettings;
    var telephony = navigator.mozTelephony;
    var vowifiStatusItem = document.querySelector('#vowifi-status-desc');

    function _initSoftKey() {
      var softkeyParams = {
        menuClassName: 'menu-button',
        header: {
          l10nId: 'message'
        },
        items: [{
          name: 'Select',
          l10nId: 'select',
          priority: 2,
          method: function() {}
        }]
      };
      SettingsSoftkey.init(softkeyParams);
      SettingsSoftkey.show();
    }

    function _switchChange() {
      var obj = {};
      if (elements.volteSwitch.value === 'true' &&
        elements.vowifiSwitch.value === 'true') {
        obj['ril.ims.enabled'] = true;
        obj['ril.ims.preferredProfile'] = 'wifi-preferred';
      } else if (elements.volteSwitch.value === 'true' &&
        elements.vowifiSwitch.value === 'false') {
        obj['ril.ims.enabled'] = true;
        obj['ril.ims.preferredProfile'] = 'cellular-only';
      } else if (elements.volteSwitch.value === 'false' &&
        elements.vowifiSwitch.value === 'true') {
        obj['ril.ims.enabled'] = true;
        obj['ril.ims.preferredProfile'] = 'wifi-only';
      } else if (elements.volteSwitch.value === 'false' &&
        elements.volteSwitch.value === 'false') {
        obj['ril.ims.enabled'] = false;
      }

      var result = _settings.createLock().set(obj);
      result.onsuccess = function () {
        var toast = {
          messageL10nId: 'changessaved',
          latency: 3000,
          useTransition: true
        };
        Toaster.showToast(toast);
        _updateDesc();
      };
      result.onerror = function () {
        console.error("Error: An error occure, can't set ims switch");
        _updateDesc();
        _updateUI();
      };
    }

    function getSetting(settingKey) {
      return new Promise(function (resolve, reject) {
        var transaction = _settings.createLock();
        var req = transaction.get(settingKey);
        req.onsuccess = function () {
          resolve(req.result[settingKey]);
        };
        req.onerror = function () {
          resolve(false);
        };
      });
    }

    function _updateDesc() {
      vowifiStatusItem.textContent = '';
      var imsEnabled = null;
      var imsProfile = null;

      var p1 = getSetting('ril.data.defaultServiceId');
      var p2 = getSetting('ril.ims.enabled');
      var p3 = getSetting('ril.ims.preferredProfile');
      Promise.all([p1, p2, p3]).then(function(values) {
        var serviceId = values[0];
        imsEnabled = values[1];
        imsProfile = values[2];
        var imsHandler = navigator.mozMobileConnections[serviceId].imsHandler;
        if (imsHandler) {
          AirplaneModeHelper.ready(function() {
            var status = AirplaneModeHelper.getStatus();
            if ((status === 'enabled')) {
              if (imsEnabled &&
                (imsProfile === 'wifi-preferred' ||
                imsProfile === 'wifi-only')) {
                if (imsHandler.unregisteredReason) {
                  _updateVowifiDesc('errorMessage');
                  vowifiStatusItem.hidden = false;
                } else {
                  _updateVowifiDesc('airplaneMode');
                  vowifiStatusItem.hidden = false;
                }
              } else {
                vowifiStatusItem.hidden = true;
              }
            } else {
              vowifiStatusItem.hidden = true;
              if (imsEnabled &&
                (imsProfile === 'wifi-preferred' ||
                imsProfile === 'wifi-only')) {
                if (imsHandler.unregisteredReason) {
                  _updateVowifiDesc('errorMessage');
                  vowifiStatusItem.hidden = false;
                }
              }
            }
          });
          if (telephony) {
            telephony.ontelephonycoveragelosing = (evt) => {
              _updateVowifiDesc('poorSignal');
            };
          }
        }
      });
    }

    function _updateVowifiDesc(status) {
      var l10n = 'volte-status-' + status;
      vowifiStatusItem.textContent = ' - '+ navigator.mozL10n.get(l10n);
    }

    function _updateUI() {
      var mobileConnection = navigator.mozMobileConnections[0];
      var vowifiElement = elements.vowifiSwitch.parentNode.parentNode;
      var volteElement = elements.volteSwitch.parentNode.parentNode;
      var volteWifiH1 = document.getElementById('volte-vowifi-h1');
      var supportedBearers =
        mobileConnection.imsHandler.deviceConfig.supportedBearers;

      if ((localStorage.getItem('isSupportWifiDevice') === 'true') &&
        supportedBearers && supportedBearers.indexOf('wifi') >= 0) {
        vowifiElement.classList.remove('hidden');
      } else {
        vowifiElement.classList.add('hidden');
        volteWifiH1.setAttribute('data-l10n-id', 'volte');
      };
      if (supportedBearers && supportedBearers.indexOf('cellular') >= 0) {
        volteElement.classList.remove('hidden');
      } else {
        volteElement.classList.add('hidden');
        volteWifiH1.setAttribute('data-l10n-id', 'vowifi');
      };

      var req = _settings.createLock().get('ril.ims.enabled');
      req.onsuccess = function() {
        var imsEnabled = req.result['ril.ims.enabled'];
        var profileReq = _settings.createLock().get(
          'ril.ims.preferredProfile'
        );
        profileReq.onsuccess = function() {
          var imsProfile = profileReq.result['ril.ims.preferredProfile'];
          if (imsEnabled && (imsProfile === 'cellular-preferred' ||
            imsProfile === 'wifi-preferred')) {
            elements.volteSwitch.value = 'true';
            elements.vowifiSwitch.value = 'true';
          } else if (imsEnabled && imsProfile === 'cellular-only') {
            elements.volteSwitch.value = 'true';
            elements.vowifiSwitch.value = 'false';
          } else if (imsEnabled && imsProfile === 'wifi-only') {
            elements.volteSwitch.value = 'false';
            elements.vowifiSwitch.value = 'true';
          } else if (!imsEnabled) {
            elements.volteSwitch.value = 'false';
            elements.vowifiSwitch.value = 'false';
          }
          window.dispatchEvent(new CustomEvent('refresh'));
        };
      };
    }

    return SettingsPanel({
      onInit: function(panel) {
        elements = {
          volteSwitch: document.getElementById('volte-switch'),
          vowifiSwitch: document.getElementById('vowifi-switch')
        };
      },

      onBeforeShow: function() {
        _updateDesc();
        _updateUI();
        elements.volteSwitch.addEventListener('change', _switchChange);
        elements.vowifiSwitch.addEventListener('change', _switchChange);
      },

      onBeforeHide: function() {
        elements.volteSwitch.addEventListener('change', _switchChange);
        elements.vowifiSwitch.addEventListener('change', _switchChange);
      },

      onShow: function() {
        _initSoftKey();
      },

      onHide: function() {
        SettingsSoftkey.hide();
      }
    });
  };
});
