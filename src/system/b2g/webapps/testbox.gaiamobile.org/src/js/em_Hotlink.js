/**
 * Created by xa on 8/5/14.
 */
'use strict';
$('menuItem-hotlink').addEventListener('click', function() {
  if(true != inited) {
    HotLink.init();
  }
});
var defpath1 = 'inhotlink';
var defpath2 = 'exhotlink';
var inited = false;
var defJSON = {
  "hotlink":[
    {"path":"http://imps.tcl-ta.com/liuqiong/media/content_e5.html","name":"imps.tcl-ta.com"},
    {"path":"http://testing.handsets.es/gp2.php","name":"testing.handsets"},
    {"path":"http://whatsmyuseragent.com/","name":"whatsmyuseragent"}
  ]
};
var HotLink = {

  init: function init(){
    inited = true;
    newItems(defJSON.hotlink);
    getDefJsonFile(defpath1);
    function newItems(hotlink){

      for(var i = 0; i < hotlink.length; i++){
        var a = document.createElement('a');
        a.id = 'a' + i;
        a.innerHTML = hotlink[i].name;
        a.title = hotlink[i].path;
        var li = document.createElement('li');
        li.appendChild(a);
        $('hotlinklist').appendChild(li);
        $(a.id).tabIndex = 1;
        $(a.id).jrdFocus = true;
        $(a.id).addEventListener('click', function(e) {
          HotLink.openGuide(e.target.title);
        });
        /*$(a.id).addEventListener('click', function() {
         debug(' a.href = ' + a.href );
         HotLink.loadURL();
         });*/
      }
    }
    function getDefJsonFile(path){
      var engmodeEx = navigator.engmodeExtension;
      var req = engmodeEx.checkIsFileExist(path);
      req.onsuccess = function(e){
        if('EXIST' == e.target.result){
          var sDefhotlink = engmodeEx.fileReadLE(path);
          var jdata = JSON.parse(sDefhotlink);
          newItems(jdata.hotlink);
        }else if ('NO_EXIST' == e.target.result){
          if(defpath2 != path){
            getDefJsonFile(defpath2);
          }
        }
      }
    }
  },

  loadJSON: function loadJSON(){
    if (!callback)
      return;
    var xhr = new XMLHttpRequest();
    xhr.onerror = function() {
      console.error('Failed to fetch file: ' + href, xhr.statusText);
    };
    xhr.onload = function() {
      callback(xhr.response);
    };
    xhr.open('GET', href, true);
    xhr.responseType = 'json';
    xhr.send();
  },
  openGuide: function openGuide(url) {
    // browse a URL
    new MozActivity({
      name: 'view',
      data: { type: 'url', url: url }
    });
  },
  loadURL:function loadURL(url){
    window.location.href = url;
  }
}
