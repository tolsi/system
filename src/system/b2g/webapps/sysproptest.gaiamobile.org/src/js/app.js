'use strict';

//only support these keys because key "Back"&"Message" are too easy to be mis-pressed
var supportedKeyCode = [
	'Enter', 
	'ArrowUp', 
	'ArrowDown',
	'ArrowLeft', 
	'ArrowRight' 
	];

//use these variables and initMovements to process movements
var elementsTable = [];
var activeElement = "selectPropertyType";

//default omaNodes for non SPR/ATT projects
var omaNodes =(function(){
	function getPropertyTypes() {
		return [];
	};

	function getPropertyIds(type) {
		return [];
	};
	
	return {
		getPropertyTypes: getPropertyTypes,
		getPropertyIds: getPropertyIds
	};
})();

function $(id) {
  return document.getElementById(id);
}

function getKeyCode(key) {
  for(var i=0; i<supportedKeyCode.length; i++) {
  	if (supportedKeyCode[i] == key)
  		return i;
  }
  return -1;
}

function onLoad() {
	console.log("onLoad");

	var operatorName = navigator.jrdExtension.readRoValue("ro.operator.name");
	console.log("onLoad, operatorName="+operatorName);
	if (operatorName === "SPR")
	{omaNodes = omaNodes_SPR;}
	else if (operatorName === "ATT")
	{omaNodes = omaNodes_ATT;}
	else
	{}

	initMovements();
	initActions();
	initSelects();
	initFocus();
}

function initFocus() {
	activeElement = "selectPropertyType";
	$(activeElement).className="active";	
}

function initMovements() {
	elementsTable = [];
	elementsTable["selectPropertyType"] = {up:null, down:"selectPropertyId", left:null, right:"selectPropertyType"};
	elementsTable["selectPropertyId"] = {up:"selectPropertyType", down:"inputPropertyValue", left:"selectPropertyType", right:"inputPropertyValue"};
	elementsTable["inputPropertyValue"] = {up:"selectPropertyId", down:"buttonGetProperty", left:"selectPropertyId", right:"buttonGetProperty"};
	elementsTable["buttonSetProperty"] = {up:"inputPropertyValue", down:null, left:"buttonGetProperty", right:null};
	elementsTable["buttonGetProperty"] = {up:"inputPropertyValue", down:"buttonSetProperty", left:"inputPropertyValue", right:"buttonSetProperty"};
}

function initActions() {
	$("buttonSetProperty").onclick=onSetProperty;
	$("buttonGetProperty").onclick=onGetProperty;
	$("selectPropertyType").onchange=function(){
		console.log("selectPropertyType.onchange");
		updateSelectPropertyId();
	};
}

function initSelects() {
	var selectEl = $("selectPropertyType");
	selectEl.options.length=0;
	var propertyTypes = omaNodes.getPropertyTypes();
	console.log("initSelects");

  for(var item in propertyTypes) {
  		var option = document.createElement("option");
  		option.text = propertyTypes[item];
  		option.setAttribute("value", propertyTypes[item]);
  		selectEl.add(option);
  }
	selectEl.selectedIndex=0;
	updateSelectPropertyId();
}

function updateSelectPropertyId() {
	var selectedType = $("selectPropertyType").selectedOptions[0];
	console.log("updateSelectPropertyId, selectedType="+selectedType.value);
	var selectEl = $("selectPropertyId");
	selectEl.options.length=0;
	var propertyIds = omaNodes.getPropertyIds(selectedType.value);
  for(var item in propertyIds) {
  		var option = document.createElement("option");
  		option.text = propertyIds[item].name;
  		option.setAttribute("value", propertyIds[item].value);
  		selectEl.add(option);
  }
	selectEl.selectedIndex=0;
}

function moveTo(target) {
	if (target) {
		$(activeElement).className="";	
		activeElement = target;
		$(activeElement).className="active";	
	}
}

function onKeyDown(e) {
	//console.log("onKeyDown, key="+e.key+"; keyCode="+getKeyCode(e.key));

	var elActive = $(activeElement);

	//console.log("elActive="+elActive.id);		
	switch(getKeyCode(e.key)) {
		case 0://enter
			if (elActive.nodeName.toLowerCase() === "input")
				elActive.value = "";
			elActive.focus();
			elActive.click();
			break;
		case 1://up
			moveTo(elementsTable[elActive.id].up);
			break;
		case 2://down
			moveTo(elementsTable[elActive.id].down);
			break;
		case 3://left
			moveTo(elementsTable[elActive.id].left);
			break;
		case 4://right
			moveTo(elementsTable[elActive.id].right);
			break;
		default: //key not supported
			break;
	}
	
}

function getSelectedPropertyId() {
	return $("selectPropertyId").selectedOptions[0].value;
}

function onGetProperty() {
	
	//let's see what property to get
	var id = getSelectedPropertyId();

	console.log("onGetProperty, id="+id);
	
	navigator.mozSysProp.getSysProp(id, 
		function(index,isSuccess,value,err) {
			console.log("index="+index+";isSuccess="+isSuccess+";value="+value+";err="+err);
			if (err === "NoError") {
				$("inputPropertyValue").value=value;
				$("textResponse").innerHTML="";
			}
			else {
				$("inputPropertyValue").value="";
				$("textResponse").innerHTML=err;
			}
		}
	);	
}

function onSetProperty() {
	console.log("onSetProperty");
	//let's see what property to set
	var id = getSelectedPropertyId();
	var value = $("inputPropertyValue").value;
	navigator.mozSysProp.setSysProp(id, value, 
		function(index,succeeded,errstr) {
			console.log("index="+index+";succeeded="+succeeded+";errstr="+errstr);
			$("textResponse").innerHTML=errstr;
		}
	);
}

window.addEventListener('load', onLoad);
window.addEventListener('keydown', onKeyDown);
//window.addEventListener('visibilitychange', onVisibilityChange);

