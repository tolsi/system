define([], function() {
    return function(e, t, n) {
        function o() {
            s.forEach(function(e) {
                console.log(e);
            }), s = [], a = 0;
        }
        function r(e, r, i) {
            var c = typeof i;
            s.push(t + ": " + e + ("number" === c || "boolean" === c || "string" === c ? ": (" + i + ")" : "") + (0 === n ? "" : ": " + r)), 
            0 === n ? o() : a || (a = setTimeout(o, 2e3));
        }
        function i(e, t) {
            return function() {
                var o = performance.now();
                0 === n && r(e, 0, arguments[0]);
                var i = t.apply(this, arguments), s = performance.now(), a = s - o;
                return n > 0 && a > n && r(e, s - o, arguments[0]), i;
            };
        }
        n = n || 0;
        var s = [], a = 0;
        n > -1 && Object.keys(e).forEach(function(t) {
            var n = e;
            "function" == typeof n[t] && (n[t] = i(t, n[t]));
        });
    };
});