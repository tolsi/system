
define('tmpl!cards/cmp/attachment_item.html',['tmpl'], function (tmpl) { return tmpl.toDom('<li class="cmp-attachment-item cmp-attachment-focusable">\n  <span class="cmp-attachment-icon" role="presentation"></span>\n  <span class="cmp-attachment-fileinfo">\n    <span dir="auto" class="cmp-attachment-filename p-pri"></span>\n    <span class="cmp-attachment-filesize p-pri"></span>\n  </span>\n</li>'); });

define('tmpl!cards/cmp/contact_menu.html',['tmpl'], function (tmpl) { return tmpl.toDom('<form class="cmp-contact-menu" role="dialog" data-type="action">\n  <header></header>\n   <menu>\n     <button class="cmp-contact-menu-edit" data-l10n-id="message-edit-menu-edit">\n    </button>\n     <button class="cmp-contact-menu-delete" data-l10n-id="message-edit-menu-delete">\n    </button>\n     <button class="cmp-contact-menu-cancel" data-l10n-id="message-multiedit-cancel">\n    </button>\n  </menu>\n</form>\n'); });

define('tmpl!cards/cmp/draft_menu.html',['tmpl'], function (tmpl) { return tmpl.toDom('<form role="dialog" class="cmp-draft-menu" data-type="action">\n  <menu>\n    <button id="cmp-draft-save" data-l10n-id="compose-draft-save"></button>\n    <button id="cmp-draft-discard" class="danger" data-l10n-id="compose-discard-confirm"></button>\n    <button id="cmp-draft-cancel" data-l10n-id="message-multiedit-cancel"></button>\n  </menu>\n</form>'); });

define('tmpl!cards/cmp/peep_bubble.html',['tmpl'], function (tmpl) { return tmpl.toDom('<div class="cmp-peep-bubble peep-bubble" dir="auto" role="button">\n  <span class="cmp-peep-name"></span>\n  <span class="cmp-peep-address collapsed"></span>\n</div>\n'); });

define('tmpl!cards/cmp/invalid_addresses.html',['tmpl'], function (tmpl) { return tmpl.toDom('<form role="dialog" data-type="confirm">\n  <section>\n    <h1 data-l10n-id="compose-invalid-addresses-title"></h1>\n    <p data-l10n-id="compose-invalid-addresses-description"></p>\n  </section>\n  <menu>\n    <button id="cmp-confirm-invalid-addresses"\n            class="confirm-dialog-ok full recommend"\n            data-l10n-id="dialog-button-ok"></button>\n  </menu>\n</form>\n'); });

define('tmpl!cards/msg/attach_confirm.html',['tmpl'], function (tmpl) { return tmpl.toDom('<form role="dialog" class="msg-attach-confirm" data-type="confirm">\n  <section>\n    <h1></h1>\n    <p></p>\n  </section>\n  <menu>\n    <button id="msg-attach-ok" class="full" data-l10n-id="dialog-button-ok"></button>\n  </menu>\n</form>'); });

define('tmpl!cards/cmp/suggestion_item.html',['tmpl'], function (tmpl) { return tmpl.toDom('<li class="suggestion-item" role="presentation">\r\n  <a class="suggestion" role="option" data-email="{{email}}" data-source="contacts" data-name="{{name}}">\r\n    <p class="name"><bdi class="ellipsis-dir-fix p-pri">{{name}}</bdi></p>\r\n    <p class="email">\r\n        <span class="description p-pri">{{type}}, </span>\r\n        <span class="email-detail p-pri">{{email}}</span>\r\n    </p>\r\n  </a>\r\n</li>\r\n'); });



define('marquee',[],function() {

/**
 * HTML Marquee in JavaScript/CSS
 * - slow scrolling of text depending on `behavior' and `timingFunction'
 *   parameters provided to the `activate' method
 * - start aligned left with delay (see marquee.css for details on classes)
 *
 * Creates a HTML element of the form:
 *  <headerNode>
 *    <div id="marquee-h-wrapper">
 *      <div id="marquee-h-text" class="marquee">
 *          Marqueed text content
 *      </div>
 *    </div>
 *  </headerNode>
 * where 'headerNode' is the node that will containt the text that needs a
 * marquee (i.e. <header>, <h1>, etc.).
 */
var Marquee = {
  /**
   * List of supported timing functions
   */
  timingFunction: ['linear', 'ease'],

  /**
   * Setup the marquee DOM structure
   * @param {string} text the string of text that requires a marquee.
   * @param {element} headerNode the DOM element parent of the text.
   */
  setup: function marquee_setup(text, headerNode) {
    this._headerNode = headerNode;

    this._headerWrapper = document.getElementById('marquee-h-wrapper');
    if (!this._headerWrapper) {
      this._headerWrapper = document.createElement('div');
      this._headerWrapper.id = 'marquee-h-wrapper';
      this._headerNode.appendChild(this._headerWrapper);
    }

    var headerText = document.getElementById('marquee-h-text');
    if (!headerText) {
      headerText = document.createElement('div');
      headerText.id = 'marquee-h-text';
      this._headerWrapper.dir = 'auto';
      this._headerWrapper.appendChild(headerText);
    }

    headerText.textContent = text;
  },

  /**
   * Activate the marquee
   * NOTE: This should only be called once the DOM structure is updated with
   *       Marquee.setup() and all created DOM elements are appended to the
   *       document, otherwise the text overflow check will not work properly.
   * @param {string} behavior the way the marquee behaves: 'scroll' (default)
   *                           for continuous right-to-left (rtl) scrolling or
   *                           'alternate' for alternating right-to-left and
   *                           left-to-right scrolling.
   * @param {string} timingFun the animation timing function: 'linear' (default)
   *                           for linear animation speed, or 'ease' for slow
   *                           start of the animation.
   */
  activate: function marquee_activate(behavior, timingFun) {
    if (!this._headerNode || !this._headerWrapper) {
      return;
    }

    // Set defaults for arguments
    var mode = behavior || 'scroll';
    var tf = timingFun || null;
    var timing = (Marquee.timingFunction.indexOf(tf) >= 0) ? tf : 'linear';
    var marqueeCssClass = 'marquee';

    var titleText = document.getElementById('marquee-h-text');
    var cssClass, width;

    // Regardless of text character alignment, still want shorter text that does
    // not span the whole element area to be aligned to match the UI. The
    // dir="auto" on titleText will still preserve the correct order of the
    // characters in the text.
    titleText.classList.remove('ltr-align');
    titleText.classList.remove('rtl-align');
    titleText.classList.add((document.dir === 'rtl' ? 'rtl' : 'ltr') +
                           '-align');

    // Check if the title text overflows, and if so, add the marquee class
    // NOTE: this can only be checked it the DOM structure is updated
    //       through Marquee.setup()
    if (this._headerWrapper.clientWidth < this._headerWrapper.scrollWidth) {
      // Track the CSS classes added to the text
      this._marqueeCssClassList = [];
      switch (mode) {
        case 'scroll':
          cssClass = marqueeCssClass + '-rtl';
          // Set the width of the marquee to match the text contents length
          width = this._headerWrapper.scrollWidth;
          titleText.style.width = width + 'px';
          // Start the marquee animation (aligned left with delay)
          titleText.classList.add(cssClass + '-start-' + timing);
          this._marqueeCssClassList.push(cssClass + '-start-' + timing);

          var self = this;
          titleText.addEventListener('animationend', function() {
            titleText.classList.remove(cssClass + '-start-' + timing);
            this._marqueeCssClassList.pop();
            // Correctly calculate the width of the marquee
            var visibleWidth = self._headerWrapper.clientWidth + 'px';
            titleText.style.transform = 'translateX(' + visibleWidth + ')';
            // Enable the continuous marquee
            titleText.classList.add(cssClass);
            this._marqueeCssClassList.push(cssClass);
          });
          break;
        case 'alternate':
          // If rtl text, then need to switch the direction of the animation.
          var dirSuffix = '';
          if (window.getComputedStyle(titleText).direction === 'rtl') {
            dirSuffix = '-rtl';
          }
          timing += dirSuffix;

          cssClass = marqueeCssClass + '-alt-';
          // Set the width of the marquee to match the text contents length
          width =
              this._headerWrapper.scrollWidth - this._headerWrapper.clientWidth;
          titleText.style.width = width + 'px';

          // Start the marquee animation (aligned left with delay)
          titleText.classList.add(cssClass + timing);
          break;
      }
    } else {
      if (!this._marqueeCssClassList) {
        return;
      }
      // Remove the active marquee CSS classes
      for (var titleCssClass in this._marqueeCssClassList) {
        titleText.classList.remove(titleCssClass);
      }

      titleText.style.transform = '';
    }
  }
};

return Marquee;

});


define('mime_to_class',[],function() {
  /**
   * Given a mime type, generates a CSS class name that uses just the first part
   * of the mime type. So, audio/ogg becomes mime-audio.
   * @param  {String} mimeType
   * @return {String} a class name usable in CSS.
   */
  return function mimeToClass(mimeType) {
    mimeType = mimeType || '';
    return 'mime-' + (mimeType.split('/')[0] || '');
  };
});


define('file_display',['require','l10n!'],function(require) {
  var mozL10n = require('l10n!');
  const kbSize = 1024;

  return {
    /**
     * Display a human-readable file size.
     */
    fileSize: function(node, sizeInBytes) {
      var fileSize;
      var unitName;
      var fileInfo = {};

      if (sizeInBytes >= kbSize * kbSize) {
        fileSize = parseFloat(sizeInBytes / kbSize / kbSize).toFixed(1);
        fileInfo = { megabytes: fileSize };
        unitName = 'attachment-size-meb';
      } else if (sizeInBytes < kbSize) {
        fileInfo = { bytes: sizeInBytes };
        unitName = 'attachment-size-byte';
      } else {
        fileSize = parseFloat(sizeInBytes / kbSize).toFixed(1);
        fileInfo = { kilobytes: fileSize };
        unitName = 'attachment-size-kib';
      }
      mozL10n.setAttributes(node, unitName, fileInfo);
    }
  };
});


define('contacts',['require'],function(require) {

  var filterFns = {
    contains: function(a, b) {
      a = a.toLowerCase();
      b = b.toLowerCase();
      return a.contains(b);
    },
    equality: function(a, b) {
      a = a.toLowerCase();
      b = b.toLowerCase();
      return a === b;
    }
  };

  function isMatch(contact, criteria, filterFn) {
    var found = {};

    outer:
    for (var i = 0, ilen = criteria.terms.length; i < ilen; i++) {
      var term = criteria.terms[i];
      for (var j = 0, jlen = criteria.fields.length; j < jlen; j++) {
        var field = criteria.fields[j];

        if (!contact[field]) {
          continue;
        }

        for (var k = 0, klen = contact[field].length; k < klen; k++) {
          var value = contact[field][k];
          if (typeof value.value !== 'undefined') {
            value = value.value;
          }

          if ((found[term] = filterFn(value.trim(), term))) {
            continue outer;
          }
        }
      }
    }

    return Object.keys(found).every(function(key) {
      return found[key];
    });
  }


  var Contacts = {
    rspaces: /\s+/,

    getCount: function() {
      return window.navigator.mozContacts.getCount();
    },

    /* callback is used to render the list of suggestions */
    findContactByString: function (filterValue, insertNode, callback) {
      var props = ['email', 'givenName', 'familyName'];
      return this.findBy({
        filterBy: props,
        filterOp: 'contains',
        filterValue: filterValue
      }, insertNode, callback);
    },

    findBy: function (filter, insertNode, callback) {
      var lower = [];
      var filterValue = (filter.filterValue || '').trim();
      var terms, request;

      if (!navigator.mozContacts || !filterValue.length) {
        setTimeout(function() {
          callback(
            typeof filter.filterValue === 'undefined' ? null :
                [], {} , insertNode
          );
        });
        return;
      }

      terms = filterValue.split(this.rspaces);

      filter.filterValue = terms.length === 1 ?
        terms[0] :
        terms.reduce(function(initial, term) {
          lower.push(term.toLowerCase());
          return term.length > initial.length ? term : initial;
        }, '');

      if (filter.filterValue.length < 3) {
        filter.filterLimit = 5;
      }

      lower.splice(lower.indexOf(filter.filterValue.toLowerCase()), 1);

      lower.push.apply(lower, terms);

      request = navigator.mozContacts.find(filter);

      request.onsuccess = function onsuccess() {
        var contacts = this.result.slice();
        var fields = ['email', 'givenName', 'familyName'];
        var criteria = { fields: fields, terms: lower };
        var results = [];
        var contact;

        if (terms.length > 1) {
          while ((contact = contacts.pop())) {
            if (isMatch(contact, criteria, filterFns.contains)) {
              results.push(contact);
            }
          }
        } else {
          results = contacts;
        }

        callback(results, {
          terms: terms
        }, insertNode);
      };

      request.onerror = function onerror() {
        this.onsuccess = this.onerror = null;
        callback(null);
      };
    }
  };

  return Contacts;
});

define('template!cards/compose.html',['template'], function(template) { return {
createdCallback: template.templateCreatedCallback,
template: template.objToFn({"id":"cards/compose.html","deps":[],"text":"<section class=\"cmp-compose-header\" role=\"region\" data-statuscolor=\"default\">\n  <header data-prop=\"headerNode\">\n    <h1 id=\"compose-header\" class=\"cmp-compose-header-label\" data-prop=\"cmpTitle\"></h1>\n  </header>\n</section>\n<div data-prop=\"scrollContainer\" class=\"scrollregion-below-header\"\n     role=\"heading\" aira-labelledby=\"compose-header\">\n  <div data-prop=\"addrBar\" class=\"cmp-envelope-bar\">\n    <div class=\"cmp-addr-bar\">\n      <div data-event=\"click:onContainerClick\"\n           class=\"cmp-envelope-line cmp-combo focusable\"\n           role=\"textbox\" data-prop=\"cmp_to\">\n        <span class=\"cmp-to-label cmp-addr-label\"\n              data-l10n-id=\"compose-to\" aria-hidden=\"true\"></span>\n        <div class=\"cmp-to-container cmp-addr-container\">\n          <div class=\"cmp-bubble-container\">\n              <input data-prop=\"toNode\"\n                     data-event=\"keydown:onAddressKeydown,input:onAddressInput,focus:onAddressFocus\"\n                     dir=\"auto\"\n                     class=\"cmp-to-text cmp-addr-text\" type=\"email\" />\n          </div>\n        </div>\n      </div>\n      <!-- XXX: spec calls for showing cc/bcc merged until selected,\n           but there is also the case where replying itself might need\n           to expand, so we are deferring that feature -->\n      <div data-event=\"click:onContainerClick\"\n           class=\"cmp-envelope-line cmp-combo collapsed\"\n           role=\"textbox\" data-l10n-id=\"cc-group\" data-prop=\"cmp_cc\">\n        <span class=\"cmp-cc-label cmp-addr-label\"\n               data-l10n-id=\"compose-cc\" aria-hidden=\"true\"></span>\n        <div class=\"cmp-cc-container cmp-addr-container\">\n          <div class=\"cmp-bubble-container\">\n            <input data-prop=\"ccNode\"\n                   data-event=\"keydown:onAddressKeydown,input:onAddressInput,focus:onAddressFocus\"\n                   dir=\"auto\"\n                   class=\"cmp-cc-text cmp-addr-text\" type=\"email\" />\n          </div>\n        </div>\n      </div>\n      <div data-event=\"click:onContainerClick\"\n           class=\"cmp-envelope-line cmp-combo collapsed\"\n           role=\"textbox\" data-l10n-id=\"bcc-group\" data-prop=\"cmp_bcc\">\n        <span class=\"cmp-bcc-label cmp-addr-label\"\n               data-l10n-id=\"compose-bcc\" aria-hidden=\"true\"></span>\n        <div class=\"cmp-bcc-container cmp-addr-container\">\n          <div class=\"cmp-bubble-container\">\n            <input data-prop=\"bccNode\"\n                   data-event=\"keydown:onAddressKeydown,input:onAddressInput,focus:onAddressFocus\"\n                   dir=\"auto\"\n                   class=\"cmp-bcc-text cmp-addr-text\" type=\"email\" />\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"cmp-envelope-line cmp-subject focusable\">\n      <span class=\"cmp-subject-label\"\n            data-l10n-id=\"compose-subject\" aria-hidden=\"true\"></span>\n      <input data-prop=\"subjectNode\"\n             data-event=\"keydown:onSubjectKeydown\"\n             dir=\"auto\"\n             class=\"cmp-subject-text\" type=\"text\" data-l10n-id=\"subject\" />\n    </div>\n    <div data-prop=\"errorMessage\" class=\"cmp-error-message collapsed\"></div>\n    <div data-prop=\"attachmentTotal\"\n         class=\"cmp-envelope-line cmp-attachment-total collapsed\"\n         aria-hidden=\"true\" data-event=\"click:goAttachmentsList\">\n      <span data-prop=\"attachmentLabel\"\n            class=\"cmp-attachment-label cmp-addr-label p-sec\"></span>\n      <span class=\"cmp-attachment-info\">\n        <span data-prop=\"attachmentsName\" class=\"cmp-attachment-name p-pri\"></span>\n        <span data-prop=\"attachmentsSize\" class=\"cmp-attachment-size p-pri\"></span>\n      </span>\n    </div>\n    <ul id=\"cmp-attachmentContainer\" data-prop=\"attachmentsContainer\" class=\"cmp-attachment-container collapsed\">\n    </ul>\n  </div>\n  <li class=\"cmp-body-li focusable\">\n    <div data-prop=\"textBodyNode\" class=\"cmp-body-text p-pri\"\n         contenteditable=\"true\" role=\"textbox\" aria-multiline=\"true\"\n         data-l10n-id=\"compose-text\" data-event=\"keydown:onBodyNodeKeydown\"></div>\n  </li>\n  <div data-prop=\"htmlBodyContainer\"\n       dir=\"auto\"\n       class=\"cmp-body-html collapsed\"\n       data-l10n-id=\"message-body-container\">\n  </div>\n</div>\n<section id=\"suggestionsContainer\" class=\"email-suggestion\" data-type=\"list\" data-prop=\"suggestionsContainer\">\n    <ul data-prop=\"suggestionsList\" class=\"contact-list contact-suggestions-list\" role=\"listbox\"></ul>\n</section>\n"})}; });

/**
 * Card definitions/logic for composition, contact picking, and attaching
 * things.  Although ideally, the picking and attaching will be handled by a
 * web activity or shared code.
 **/

/*global MozActivity, NavigationMap */

define('cards/compose',['require','exports','module','tmpl!./cmp/attachment_item.html','tmpl!./cmp/contact_menu.html','tmpl!./cmp/draft_menu.html','tmpl!./cmp/peep_bubble.html','tmpl!./cmp/invalid_addresses.html','tmpl!./msg/attach_confirm.html','tmpl!./cmp/suggestion_item.html','evt','html_cache','toaster','model','iframe_shims','marquee','l10n!','cards','mime_to_class','file_display','contacts','./base','template!./compose.html','./editor_mixins'],function(require, exports, module) {

var cmpAttachmentItemNode = require('tmpl!./cmp/attachment_item.html'),
    cmpContactMenuNode = require('tmpl!./cmp/contact_menu.html'),
    cmpDraftMenuNode = require('tmpl!./cmp/draft_menu.html'),
    cmpPeepBubbleNode = require('tmpl!./cmp/peep_bubble.html'),
    cmpInvalidAddressesNode = require('tmpl!./cmp/invalid_addresses.html'),
    msgAttachConfirmNode = require('tmpl!./msg/attach_confirm.html'),
    suggestionItem = require('tmpl!./cmp/suggestion_item.html'),

    evt = require('evt'),
    htmlCache = require('html_cache'),
    toaster = require('toaster'),
    model = require('model'),
    iframeShims = require('iframe_shims'),
    Marquee = require('marquee'),
    mozL10n = require('l10n!'),
    cards = require('cards'),
    mimeToClass = require('mime_to_class'),
    fileDisplay = require('file_display'),
    Contacts = require('contacts'),
    dataIdCounter = 0;

/**
 * Max composer attachment size is defined as 5120000 bytes.
 */
var MAX_ATTACHMENT_SIZE = 5120000;

/**
 * To make it easier to focus input boxes, we have clicks on their owning
 * container cause a focus event to occur on the input.  This method helps us
 * also position the cursor based on the location of the click so the cursor
 * can end up at the edges of the input box which could otherwise be very hard
 * to do.
 */
function focusInputAndPositionCursorFromContainerClick(event, input) {
  // Do not do anything if the event is happening on the input already or we
  // will disrupt the default positioning logic!  We use explicitOriginalTarget
  // because under Gecko originalTarget may contain anonymous content.
  if (event.explicitOriginalTarget === input) {
    return;
  }
  // Stop bubbling to avoid our other focus-handlers!
  event.stopPropagation();

  // coordinates are relative to the viewport origin
  var bounds = input.getBoundingClientRect();
  var midX = bounds.left + bounds.width / 2;
  // and that's what clientX is too!
  input.focus();
  var cursorPos = 0;
  if (event.clientX >= midX) {
    cursorPos = input.value.length;
  }
  input.setSelectionRange(cursorPos, cursorPos);
}

var needRefresh = false;
var doActive = true;
var draftConfirmShown = false;
var instance;
var cmp_params;
var focusTo;
var endkeyPopUp = false;
var suggestionList = false;
var suggestionListNavListener = false;

return [
  require('./base')(require('template!./compose.html')),
  require('./editor_mixins'),
  {
    createdCallback: function() {
      // Save a cached version before anything is changed on the pristine
      // template state.
      htmlCache.cloneAndSave(module.id, this);

      this.sending = false;

      // Management of attachment work, to limit memory use
      this._totalAttachmentsFinishing = 0;
      this._totalAttachmentsDone = 0;
      this._wantAttachment = false;
      this._onAttachmentDone = this._onAttachmentDone.bind(this);

      // Pass text node to editor mixins
      this._bindEditor(this.textBodyNode);

      // Add subject focus for larger hitbox
      var subjectContainer = this.querySelector('.cmp-subject');
      subjectContainer.addEventListener('click', function subjectFocus(evt) {
        focusInputAndPositionCursorFromContainerClick(
          evt, subjectContainer.querySelector('input'));
      });

      // Tracks if the card closed itself, in which case
      // no draft saving is needed. If something else
      // causes the card to die, then we want to save any
      // state.
      this._selfClosed = false;

      // Set up unique data IDs for data-sensitive operations that could be in
      // progress. These IDs are unique per kind of action, not unique per
      // instance of a kind of action. However, these IDs are just used to know
      // if a hard shutdown should be delayed a bit, and are unique enough for
      // those purposes.
      var dataId = module.id + '-' + (dataIdCounter += 1);
      this._dataIdSaveDraft = dataId + '-saveDraft';
      this._dataIdSendEmail = dataId + '-sendEmail';
    },

    onArgs: function(args) {
      this.composer = args.composer;
      this.composerData = args.composerData || {};
      this.type = args.type;
      this.activity = args.activity;
      instance = this;
      this.updateTitle(args);
      if ((this.type && this.type === 'reply') ||
          (this.activity && this.activity.source.data)) {
        this.toNode.classList.add('collapsed');
        this.ccNode.classList.add('collapsed');
        this.bccNode.classList.add('collapsed');
      }
    },

    updateTitle: function(args) {
      if (args.title) {
        mozL10n.setAttributes(this.cmpTitle, args.title);
      } else {
        mozL10n.setAttributes(this.cmpTitle, 'compose-header-short');
      }
    },

    menuHandler: function(evt) {
      var action = evt.detail.menuVisible;
      if (action === false) {
        setTimeout(() => {
          instance.refresh();
        });
      }
    },

    keydownHandler: function(evt) {
      switch (evt.key) {
        case 'Backspace':
          evt.preventDefault();
          if (NavigationMap.confirmShown) {
            NavigationMap.confirmShown = false;
          }
          if (option.menuVisible === false) {
            if (draftConfirmShown) {
              instance.hideDialog();
              if (!endkeyPopUp) {
                NavigationMap.restoreFocus();
              } else {
                endkeyPopUp = false;
                cards._endKeyClicked = false;
                option.show();
                NavigationMap.setFocus('restore');
              }
            } else {
              instance.onBack();
            }
          } else {
            if (draftConfirmShown && endkeyPopUp) {
              instance.hideDialog();
              endkeyPopUp = false;
              cards._endKeyClicked = false;
              option.show();
              NavigationMap.setFocus('restore');
            }
          }
          break;
      }
    },

    endkeyHandler: function(evt) {
      var callback = evt.detail.callback;
      endkeyPopUp = true;
      instance.onBack(callback);
    },

    onCardVisible: function(navDirection) {
      var CARD_NAME = this.localName;
      var QUERY_CHILD = '.focusable';
      var CONTROL_ID = CARD_NAME + ' ' + QUERY_CHILD;
      var source = this.activity ? this.activity.source : null;

      window.addEventListener('menuEvent', this.menuHandler);
      window.addEventListener('keydown', this.keydownHandler);
      window.addEventListener('email-endkey', this.endkeyHandler);
      if (navDirection === 'forward') {
        NavigationMap.navSetup(CARD_NAME, QUERY_CHILD);
        NavigationMap.setCurrentControl(CONTROL_ID);
        if ((this.type && this.type === 'reply') || (source && source.data &&
            Object.keys(source.data).length)) {
          // if the compose type is 'reply', we need always set focus on text
          // body node.
          if (this.htmlBodyContainer.classList.contains('collapsed')) {
            NavigationMap.setFocus('last');
          } else {
            var length = NavigationMap.getCurrentControl().elements.length;
            NavigationMap.setFocus(length - 2);
          }
          this.textBodyNode.focus();
        } else {
          NavigationMap.setFocus('first');
        }
      } else if (navDirection === 'back') {
        NavigationMap.setCurrentControl(CONTROL_ID);
        NavigationMap.setFocus('restore');
      }

      this.cmp_to.addEventListener('focus', function(e) {
        this.toNode.focus();
        this.toNode.setSelectionRange(this.toNode.value.length,
            this.toNode.value.length);
      }.bind(this));

      this.cmp_cc.addEventListener('focus', function(e) {
        this.ccNode.focus();
        this.ccNode.setSelectionRange(this.ccNode.value.length,
            this.ccNode.value.length);
      }.bind(this));

      this.cmp_bcc.addEventListener('focus', function(e) {
        this.bccNode.focus();
        this.bccNode.setSelectionRange(this.bccNode.value.length,
            this.bccNode.value.length);
      }.bind(this));

      var cmpBodyLi = document.querySelector('.cmp-body-li');
      cmpBodyLi.addEventListener('focus', function(e) {
        this.textBodyNode.focus();
      }.bind(this));

      NavigationMap.checkStorage();
    },

    onFocusChanged: function(queryChild, index, item) {
      console.log(this.localName + '.onFocusChanged, queryChild=' +
                  queryChild + ', index=' + index);
      if (item.classList.contains('cmp-combo')) {
        var inputItem =
            item.lastElementChild.lastElementChild.lastElementChild;
        if (inputItem && inputItem.classList.contains('collapsed')) {
          inputItem.classList.remove('collapsed');
        }
      }
      this.updateSoftkey();
    },

    updateSoftkey: function() {
      if (this.addATA && this.addATA.readyState === 'pending') {
        return;
      }
      var params = [];
      var focused = document.querySelector('.focus') ||
          NavigationMap.getCurrentItem();

      if (focused.classList.contains('cmp-body-li')) {
        params.push({
          name: 'Enter',
          l10nId: 'enter',
          priority: 2
        });
      } else if (focused.classList.contains('cmp-attachment-total')) {
        params.push({
          name: 'Select',
          l10nId: 'select',
          priority: 2
        });
      }
      if (focused.classList.contains('cmp-combo')) {
        focused.querySelector('.cmp-addr-text').focus();
        params.push({
          name: 'Add Contact',
          l10nId: 'add-contact',
          priority: 3,
          method: function() {
            instance.onContactAdd();
          }
        });
      } else {
        if (focused.classList.contains('cmp-subject')) {
          focused.querySelector('input').focus();
        }
        params.push({
          name: 'Add Attachment',
          l10nId: 'add-attachemnt',
          priority: 5,
          method: function() {
            instance.onAttachmentAdd();
          }
        });

        // It is the controller for Cc option
        if (this.cmp_cc.classList.contains('collapsed')) {
          params.push({
            name: 'Add Cc',
            l10nId: 'add-cc',
            priority: 5,
            method: function() {
              instance.onccAdd();
            }
          });
        } else {
          params.push({
            name: 'Remove Cc',
            l10nId: 'remove-cc',
            priority: 5,
            method: function() {
              instance.onccRemove();
            }
          });
        }

        // It is the controller for Bcc option
        if (this.cmp_bcc.classList.contains('collapsed')) {
          params.push({
            name: 'Add Bcc',
            l10nId: 'add-bcc',
            priority: 5,
            method: function() {
              instance.onbccAdd();
            }
          });
        } else {
          params.push({
            name: 'Remove Bcc',
            l10nId: 'remove-bcc',
            priority: 5,
            method: function() {
              instance.onbccRemove();
            }
          });
        }

        params.push({
          name: 'Save As Draft',
          l10nId: 'save-as-draft',
          priority: 5,
          method: function() {
            instance._saveDraft('explicit', () => {
              instance.goBack();
            });
            toaster.toast({
              text: mozL10n.get('composer-draft-saved')
            });
          }
        });
        params.push({
          name: 'Cancel',
          l10nId: 'cancel',
          priority: 5,
          method: function() {
            instance.onBack();
          }
        });
      }
      cmp_params = params;
      this.setSoftkey();
    },

    setSoftkey: function() {
      var params = [];
      if (this.needSendKey) {
        params.push({
          name: 'Send',
          l10nId: 'compose-send',
          priority: 1,
          method: function() {
            instance.onSend();
          }
        });
      }
      if (cmp_params) {
        NavigationMap.setSoftKeyBar(params.concat(cmp_params));
      }
    },

    switchSKtoSuggetion: function() {
      var params = [];
      params.push({
        name: 'Select',
        l10nId: 'select',
        priority: 2,
        method: function() {
          var node = instance.suggestionsList.querySelector('.selected');
          node.click();
        }
      });
      NavigationMap.setSoftKeyBar(params);
    },

    /**
     * Inform Cards to not emit startup content events, this card will trigger
     * them once data from back end has been received and the DOM is up to date
     * with that data.
     * @type {Boolean}
     */
    skipEmitContentEvents: true,

    /**
     * Focus our contenteditable region and position the cursor at the last
     * valid editing cursor position.
     *
     * The intent is so that if the user taps below our editing region that we
     * still correctly position the cursor.  We previously relied on min-height
     * to do this for us, but that results in ugly problems when we have quoted
     * HTML that follows and our editable region is not big enough to satisfy
     * the height.
     *
     * Note: When we are quoting HTML, the "Bob wrote:" stuff does go in the
     * contenteditable text area, so we may actually want to get smarter and
     * position the cursor before that node instead.
     */
    _focusEditorWithCursorAtEnd: function(event) {
      if (event) {
        event.stopPropagation();
      }

      // Selection/range manipulation is the easiest way to force the cursor
      // to a specific location.
      //
      // Note: Once the user has pressed return once, the editor will create a
      // bogus <br type="_moz"> that is always the last element.  Even though
      // this bogus node will be the last child, nothing tremendously bad
      // happens.
      //
      // Note: This technique does result in our new text existing in its own,
      // new text node.  So don't make any assumptions about how text nodes are
      // arranged.
      var insertAfter = this.textBodyNode.lastChild;
      var range = document.createRange();
      range.setStartAfter(insertAfter);
      range.setEndAfter(insertAfter);

      this.textBodyNode.focus();
      var selection = window.getSelection();
      selection.removeAllRanges();
      selection.addRange(range);
    },


    postInsert: function() {
      // the HTML bit needs us linked into the DOM so the iframe can be
      // linked in, hence this happens in postInsert.
      require(['iframe_shims'], function() {

        // NOTE: when the compose card changes to allow switching the From
        // account then this logic will need to change, both the acquisition of
        // the account pref and the folder to use for the composer. So it is
        // good to group this logic together, since they both will need to
        // change later.
        if (this.composer) {
          this._loadStateFromComposer();
        } else {
          var data = this.composerData;
          model.latestOnce('folder', function(folder) {
            this.composer = model.api.beginMessageComposition(data.message,
                                                              folder,
                                                              data.options,
                                                              function() {
              if (data.onComposer) {
                data.onComposer(this.composer, this);
              }

              this._loadStateFromComposer();
            }.bind(this));
          }.bind(this));
        }
      }.bind(this));
    },

    _loadStateFromComposer: function() {
      var self = this;
      function expandAddresses(node, addresses) {
        if (!addresses) {
          return '';
        }
        addresses.forEach(function(aval) {
          var name, address;
          if (typeof(aval) === 'string') {
            // TODO: We will apply email address parser for showing bubble
            //       properly. We set both name and address same as aval string
            //       before parser is ready.
            name = address = aval;
          } else {
            name = aval.name;
            address = aval.address;
          }
          self.insertBubble(node, name, address);
        });
      }
      expandAddresses(this.toNode, this.composer.to);

      expandAddresses(this.ccNode, this.composer.cc);
      if (this.composer.cc && this.composer.cc.length > 0) {
        this.cmp_cc.classList.remove('collapsed');
        this.cmp_cc.classList.add('focusable');
      }

      expandAddresses(this.bccNode, this.composer.bcc);
      if (this.composer.bcc && this.composer.bcc.length > 0) {
        this.cmp_bcc.classList.remove('collapsed');
        this.cmp_bcc.classList.add('focusable');
      }

      this.validateAddresses();
      doActive = false;
      this.renderSendStatus();

      // Add attachments
      this.renderAttachments();

      this.subjectNode.value = this.composer.subject;
      // Save the initial state of the composer so that if the user immediately
      // hits the back button without doing anything we can simply discard the
      // draft. This is not for avoiding redundant saves or any attempt at
      // efficiency.
      this.origText = this.composer.body.text;

      this.populateEditor(this.composer.body.text);

      if (this.composer.body.html) {
        this.htmlBodyContainer.classList.remove('collapsed');
        this.htmlBodyContainer.classList.add('focusable');
        // Although (still) sanitized, this is still HTML we did not create and
        // so it gets to live in an iframe.  Its read-only and the user needs to
        // be able to see what they are sending, so reusing the viewing
        // functionality is desirable.
        var ishims = iframeShims.createAndInsertIframeForContent(
          this.composer.body.html, this.scrollContainer,
          this.htmlBodyContainer, /* append */ null,
          'noninteractive',
          /* no click handler because no navigation desired */ null);
        this.htmlIframeNode = ishims.iframe;

        this.htmlBodyContainer.addEventListener('keydown', function(evt) {
          switch (evt.key) {
            case 'ArrowDown':
              instance.scrollByStep(instance.scrollContainer, 'down');
              evt.preventDefault();
              evt.stopPropagation();
              break;
            case 'ArrowUp':
              // we need to check if we should move the focus out of message
              // body node or not
              if (instance.textBodyNodeIsVisible()) {
                return;
              }
              instance.scrollByStep(instance.scrollContainer, 'up');
              evt.preventDefault();
              evt.stopPropagation();
              break;
          }
        });
      }

      // There is a bit more possibility of async work done in the iframeShims
      // internals, but this is close enough and is better than breaking open
      // the internals of the iframeShims to get the final number.
      if (!this._emittedContentEvents) {
        evt.emit('metrics:contentDone');
        this._emittedContentEvents = true;
      }
    },

    scrollByStep: function(el, dir) {
      var sHeight = el.scrollHeight;
      var cHeight = el.clientHeight;
      var moveHeight = sHeight - cHeight;
      var sTop = el.scrollTop;
      var stepHeight = document.documentElement.clientHeight / 8;
      if (dir === 'down') {
        if (sTop < moveHeight) {
          if (sTop + stepHeight >= moveHeight) {
             el.scrollTop = moveHeight;
          } else {
             el.scrollTop = sTop + stepHeight;
          }
        }
      } else if (dir === 'up') {
        if (sTop - stepHeight >= 0) {
          el.scrollTop = sTop - stepHeight;
        } else {
          el.scrollTop = 0;
        }
      }
    },

    textBodyNodeIsVisible: function() {
      var rects = this.textBodyNode.getClientRects();
      var top = rects[0].top;

      if (top > this.scrollContainer.offsetTop) {
        return true;
      } else {
        return false;
      }
    },

    /**
     * If this draft came from the outbox, it might have a sendStatus
     * description explaining why the send failed. Display it if so.
     *
     * The sendStatus information on this messages is provided through
     * the sendOutboxMessages job; see `jobs/outbox.js` in GELAM for details.
     */
    renderSendStatus: function() {
      var sendStatus = this.composer.sendStatus || {};
      if (sendStatus.state === 'error') {
        var badAddresses = sendStatus.badAddresses || [];

        // For debugging, report some details to the console, masking
        // recipients for privacy.
        console.log('Editing a failed outbox message. Details:',
        JSON.stringify({
          err: sendStatus.err,
          badAddressCount: badAddresses.length,
          sendFailures: sendStatus.sendFailures
        }, null, ' '));

        var l10nId;
        if (badAddresses.length || sendStatus.err === 'bad-recipient') {
          l10nId = 'send-failure-recipients';
        } else {
          l10nId = 'send-failure-unknown';
        }

        this.errorMessage.setAttribute('data-l10n-id', l10nId);
        this.errorMessage.classList.remove('collapsed');
      } else {
        this.errorMessage.classList.add('collapsed');
      }
    },

    /**
     * Return true if the given address is syntactically valid.
     *
     * @param {String} address
     *   The email address to validate, as a string.
     * @return {Boolean}
     */
    isValidAddress: function(address) {
      // An address is valid if model.api.parseMailbox thinks it
      // contains a valid address. (It correctly classifies names that
      // are not valid addresses.)
      var mailbox = model.api.parseMailbox(address);
      return mailbox && mailbox.address;
    },

    /**
     * Extract addresses from the bubbles and/or inputs, returning a map
     * with keys for 'to', 'cc', 'bcc', 'all', and 'invalid' addresses.
     */
    extractAddresses: function() {
      var allAddresses = [];
      var invalidAddresses = [];

      // Extract the addresses from the bubbles as well as any partial
      // addresses entered in the text input.
      var frobAddressNode = (function(node) {
        var bubbles = node.parentNode.querySelectorAll('.cmp-peep-bubble');
        var addrList = [];
        for (var i = 0; i < bubbles.length; i++) {
          var dataSet = bubbles[i].dataset;
          addrList.push({ name: dataSet.name, address: dataSet.address });
        }
        if (node.value.trim().length !== 0) {
          var mailbox = model.api.parseMailbox(node.value);
          addrList.push({ name: mailbox.name, address: mailbox.address });
        }
        addrList.forEach(function(addr) {
          allAddresses.push(addr);
          if (!this.isValidAddress(addr.address)) {
            invalidAddresses.push(addr);
          }
        }.bind(this));
        return addrList;
      }.bind(this));

      // NOTE: allAddresses contains invalidAddresses, but we never
      // actually send a message directly using either of those lists.
      // We use to/cc/bcc for that, and our send validation here
      // prevents users from sending a message with invalid addresses.

      return {
        to: frobAddressNode(this.toNode),
        cc: frobAddressNode(this.ccNode),
        bcc: frobAddressNode(this.bccNode),
        all: allAddresses,
        invalid: invalidAddresses
      };
    },

    _saveStateToComposer: function() {
      var addrs = this.extractAddresses();
      this.composer.to = addrs.to;
      this.composer.cc = addrs.cc;
      this.composer.bcc = addrs.bcc;
      this.composer.subject = this.subjectNode.value;
      this.composer.body.text = this.fromEditor();
      // The HTML representation cannot currently change in our UI, so no
      // need to save it.  However, what we send to the back-end is what gets
      // sent, so if you want to implement editing UI and change this here,
      // go crazy.
    },

    _closeCard: function() {
      this._selfClosed = true;
      cards.removeCardAndSuccessors(this, 'animate');
    },

    _saveNeeded: function() {
      var self = this;
      var checkAddressEmpty = function() {
        var bubbles = self.querySelectorAll('.cmp-peep-bubble');
        if (bubbles.length === 0 && !self.toNode.value && !self.ccNode.value &&
            !self.bccNode.value) {
          return true;
        } else {
          return false;
        }
      };

      // If no composer, then it means the card was destroyed before full
      // setup, which means there is nothing to save.
      if (!this.composer) {
        return false;
      }

      var hasNewContent = this.fromEditor() !== this.composer.body.text;

      // We need `to save / ask about deleting the draft if:
      // There's any recipients listed, there's a subject, there's anything in
      // the body, there are attachments, or we already created a draft for this
      // guy in which case we really want to provide the option to delete the
      // draft.
      return (this.subjectNode.value || hasNewContent ||
          !checkAddressEmpty() || this.composer.attachments.length ||
          this.composer.hasDraft);
    },

    _saveDraft: function(reason, callback) {
      // If the send process is happening, suppress automatic saves.
      // (Manual saves should not happen when 'sending' is true, but breaking
      // auto-saves would be very bad form.)
      if (this.sending && reason === 'automatic') {
        console.log('compose: skipping autosave because send in progress');
        return;
      }
      this._saveStateToComposer();
      evt.emit('uiDataOperationStart', this._dataIdSaveDraft);
      this.composer.saveDraft(function() {
        evt.emit('uiDataOperationStop', this._dataIdSaveDraft);
        if (callback) {
          callback();
        }
      }.bind(this));
    },

    createBubbleNode: function(name, address) {
      var bubble = cmpPeepBubbleNode.cloneNode(true);
      bubble.classList.add('peep-bubble');
      bubble.classList.add('msg-peep-bubble');
      bubble.setAttribute('data-address', address);
      bubble.querySelector('.cmp-peep-address').textContent = address;
      var nameNode = bubble.querySelector('.cmp-peep-name');
      if (navigator.largeTextEnabled) {
        nameNode.classList.add('p-pri');
      }
      if (!name) {
        nameNode.textContent = address.indexOf('@') !== -1 ?
                      address.split('@')[0] : address;
      } else {
        nameNode.textContent = name;
        bubble.setAttribute('data-name', name);
      }
      return bubble;
    },

    /**
     * insertBubble: We can set the input text node, name and address to
     *               insert a bubble before text input.
     */
    insertBubble: function(node, name, address) {
      var container = node.parentNode;
      var bubble = this.createBubbleNode(name || address, address);
      var bInsert = true;
      for (var i = 0; i < container.children.length - 1; i++) {
        var dataSet = container.children[i].dataset;
        if (dataSet.address === address) {
          bInsert = false;
          break;
        }
      }
      if (bInsert) {
        container.insertBefore(bubble, node);
        this.validateAddresses();
      }
    },

    /**
     * deleteBubble: Delete the bubble from the parent container.
     */
    deleteBubble: function(node) {
      if (!node) {
        return;
      }
      var container = node.parentNode;
      if (node.classList.contains('cmp-peep-bubble')) {
        container.removeChild(node);
      }

      this.validateAddresses();
    },

    /**
     * editBubble: Turn the bubble back into editable text.
     */
    editBubble: function(node) {
      if (!node) {
        return;
      }
      var container = node.parentNode;
      if (node.classList.contains('cmp-peep-bubble')) {
        container.removeChild(node);
        var input = container.querySelector('.cmp-addr-text');
        // If there is already a partially or fully entered address in
        // the typing area, force it to be converted into a bubble, even
        // though the resulting address may not be valid. If it's not
        // valid, that bubble can subsequently be edited. This helps
        // avoid the user losing anything they typed in.
        if (input.value.length > 0) {
          input.value = input.value + ',';
          this.onAddressInput({ target: input }); // Bubblize if necessary.
        }
        var address = node.dataset.address;
        var selStart = input.value.length;
        var selEnd = selStart + address.length;
        input.value += address;
        input.focus();
        this.onAddressInput({ target: input }); // Force width calculations.
        input.setSelectionRange(selStart, selEnd);
      }
      this.validateAddresses();
    },

    /**
     * Handle bubble deletion while keyboard backspace keydown.
     */
    onAddressKeydown: function(evt) {
      var node = evt.target;

      if (evt.keyCode === 8 && evt.key === 'Backspace') {
        var previousBubble = node.previousElementSibling;
        if (node.value === '' && previousBubble) {
          this.deleteBubble(previousBubble);
          evt.preventDefault();
          evt.stopPropagation();
        }
      }

      if (evt.key === 'Enter') {
        if (!this.isValidAddress(node.value)) {
          var dialogConfig = {
            title: {
              id: 'compose-invalid-addresses-title',
              args: {}
            },
            body: {
              id: 'compose-invalid-addresses-description',
              args: {}
            },
            desc: {
              id: '',
              args: {}
            },
            accept: {
              l10nId: 'dialog-button-ok',
              priority: 2,
              callback: function() {
                var index = NavigationMap.getCurrentControl().index;
                NavigationMap.setFocus(index);
              }
            }
          };
          evt.preventDefault();
          evt.stopPropagation();
          var dialog = new ConfirmDialogHelper(dialogConfig);
          dialog.show(document.getElementById('confirm-dialog-container'));
        } else {
          // This 0 time out is in order to handle 'Enter' key asynchronous.
          // Because when press 'Enter' key very quickly during input email address,
          // if not wait the input finish event handled, the input node's value
          // cannot be set as '' in onAddressInput.
          setTimeout(()=> {
            if (node.value) {
              node.value = node.value + ',';
              instance.onAddressInput(evt);
            }
          });
        }
      }

      if (evt.key === 'ArrowDown' ||
          (evt.key === 'ArrowUp' && !node.classList.contains('cmp-to-text'))) {
        if (node.classList.contains('cmp-addr-text')) {
          if (node.value) {
            if (instance.isValidAddress(node.value) && !instance.suggestionList) {
              node.value = node.value + ',';
              instance.onAddressInput(evt);
            }
          }
          if (node.value === '' &&
              node.parentNode.children.length > 1) {
            node.classList.add('collapsed');
          }
        }
      }

      // due to old symbol table has been removed, and new IME's
      // symbol table is not ready yet, so remove old symbol table logic here,
      // after new IME's symbol table is landed, will add the logic back.
      //
      // var symbol = window.SymbolInput;

      if (evt.key === 'ArrowDown') {
        var index = NavigationMap.getCurrentControl().index;
        if (suggestionList) {
          var focused = document.querySelectorAll('.focus');
          for (var i = 0; i < focused.length; i++) {
            focused[i].classList.remove('focus');
          }
          document.activeElement.blur();
        }
      }
    },

    onSubjectKeydown: function(evt) {
      if (evt.key === 'Enter') {
        evt.preventDefault();
        evt.stopPropagation();
      }
    },

    startNavOnSuggestionList: function(evt) {
      window.addEventListener('keydown', this.handleNavOnSuggestionList);
      suggestionListNavListener = true;
    },

    stopNavOnSuggestionList: function(evt) {
      window.removeEventListener('keydown', this.handleNavOnSuggestionList);
      suggestionListNavListener = false;
    },

    handleNavOnSuggestionList: function(evt) {
      var selected = instance.suggestionsList.querySelector('.selected');
      switch (evt.key) {
        case 'ArrowDown':
          evt.preventDefault();
          evt.stopPropagation();
          var index = NavigationMap.getCurrentControl().index;
          if (selected) {
            selected.classList.remove('selected');
            if (selected.nextElementSibling) {
              selected.nextElementSibling.classList.add('selected');
              instance.suggestionsList.querySelector('.selected').
                  scrollIntoView(false);
            } else {
              instance.clearSuggestionList();
              instance.stopNavOnSuggestionList();
              NavigationMap.setFocus(index + 1);
              NavigationMap.scrollToElement(NavigationMap.getCurrentItem(),
                  { key: 'ArrowDown' });
            }
          } else {
            instance.suggestionsList.children[0].classList.add('selected');
            instance.switchSKtoSuggetion();
          }
          break;
        case 'ArrowUp':
          evt.preventDefault();
          evt.stopPropagation();
          var index = NavigationMap.getCurrentControl().index;
          if (selected) {
            selected.classList.remove('selected');
            if (selected.previousElementSibling) {
              selected.previousElementSibling.classList.add('selected');
              instance.suggestionsList.querySelector('.selected').
                  scrollIntoView(true);
            } else {
              NavigationMap.setFocus(index);
              NavigationMap.scrollToElement(NavigationMap.getCurrentItem(),
                  { key: 'ArrowUp' });
              instance.setSoftkey();
            }
          }
          break;
      }
    },

    onAddressFocus: function(evt) {
      evt.target.setSelectionRange(9999,9999);
      instance.searchContacts(evt);
    },

    /**
     * Handle bubble creation while keyboard comma input.
     */
    onAddressInput: function(evt) {
      this.searchContacts(evt);

      if (evt.isComposing) {
        return;
      }

      var node = evt.target;
      var makeBubble = false;
      // When do we want to tie off this e-mail address, put it into a bubble
      // and clear the input box so the user can type another address?
      switch (node.value.slice(-1)) {
        // If they hit space and we believe they've already typed an email
        // address!  (Space is okay in a display name or to delimit a display
        // name from the e-mail address)
        //
        // We use the presence of an '@' character as indicating that the e-mail
        // address
        case ' ':
          makeBubble = node.value.indexOf('@') !== -1;
          break;
        // We started out supporting comma, but now it's not on our keyboard at
        // all in type=email mode!  We aren't terribly concerned about it not
        // being usable in display names, although we really should check for
        // quoting...
        case ',':
        // Semicolon is on the keyboard, and we also don't care about it not
        // being usable in display names.
        case ';':
          makeBubble = true;
          break;
      }
      if (makeBubble) {
        var mailbox = model.api.parseMailbox(node.value);
        this.insertBubble(node, mailbox.name, mailbox.address);
        node.value = '';
      }
      // XXX: Workaround to get the length of the string. Here we create a dummy
      //      div for computing actual string size for changing input
      //      size dynamically.
      if (!this.stringContainer) {
        this.stringContainer = document.createElement('div');
        this.appendChild(this.stringContainer);

        var inputStyle = window.getComputedStyle(node);
        this.stringContainer.style.fontSize = inputStyle.fontSize;
      }
      this.stringContainer.style.display = 'inline-block';
      this.stringContainer.style.visibility = 'hidden';
      this.stringContainer.textContent = node.value;
      node.style.width = (this.stringContainer.clientWidth + 2) + 'px';
      firstTimein = false;//Added by lijuanli@t2mobile.com for bug165
      this.validateAddresses();
    },

    onContainerClick: function(evt) {
      var target = evt.target;
      // Popup the context menu if clicked target is peer bubble.
      if (target.classList.contains('cmp-peep-bubble')) {
        var contents = cmpContactMenuNode.cloneNode(true);
        var email = target.querySelector('.cmp-peep-address').textContent;
        var headerNode = contents.getElementsByTagName('header')[0];
        // Setup the marquee structure
        Marquee.setup(email, headerNode);
        // Activate marquee once the contents DOM are added to document
        document.body.appendChild(contents);
        Marquee.activate('alternate', 'ease');

        var formSubmit = (function(evt) {
          document.body.removeChild(contents);
          switch (evt.explicitOriginalTarget.className) {
            case 'cmp-contact-menu-edit':
              this.editBubble(target);
              break;
            case 'cmp-contact-menu-delete':
              this.deleteBubble(target);
              break;
            case 'cmp-contact-menu-cancel':
              break;
          }
          return false;
        }).bind(this);
        contents.addEventListener('submit', formSubmit);
        return;
      }
      // While user clicks on the container, focus on input to triger
      // the keyboard.
      var input = evt.currentTarget.getElementsByClassName('cmp-addr-text')[0];
      focusInputAndPositionCursorFromContainerClick(evt, input);
    },

    /**
     * Helper to show the appropriate error when we refuse to add attachments.
     */
    _warnAttachmentSizeExceeded: function(numAttachments) {
      var title = 'attention';
      var content;

      if (numAttachments > 1) {
        // Note! attachments with an "s" versus the case below.
        content = 'compose-attchments-size-exceeded';
      } else {
        content = 'compose-attchment-size-exceeded';
      }
      document.activeElement.blur();
      var dialogConfig = {
        title: {
          id: title,
          args: {}
        },
        body: {
          id: content,
          args: {}
        },
        desc: {
          id: '',
          args: {}
        },
        accept: {
          l10nId:'confirm-dialog-ok',
          priority:2,
          callback: function() {
            var index = NavigationMap.getCurrentControl().index;
            NavigationMap.setFocus(index);
          }
        }
      };
      var dialog = new ConfirmDialogHelper(dialogConfig);
      dialog.show(document.getElementById('confirm-dialog-container'));
      setTimeout( () => {
        window.focus();
      }, 80);
    },

    /**
     * Used to count when attachment has been fully processed by this.composer.
     * Broken out as a separate member method to avoid inline closures in
     * addAttachmentsSubjectToSizeLimits that may lead to holding on to too much
     * memory.
     */
    _onAttachmentDone: function() {
      this._totalAttachmentsDone += 1;
      if (this._totalAttachmentsDone < this._totalAttachmentsFinishing) {
        return;
      }

      // Give a bit of time for all the DB transactions to clean up.
      // Unfortunately there are no good signals to do this decisively so just
      // adding a bit of a buffer, just to be nice for super low memory
      // devices. Not a catastrophe if work is still going on when the timeout
      // fires.
      setTimeout(function() {
        var wantAttachment = this._wantAttachment;
        this._totalAttachmentsFinishing = 0;
        this._totalAttachmentsDone = 0;
        this._wantAttachment = false;

        // Close out the toaster if it was showing. While the toaster could
        // be showing for some other reason, this is the most likely cause,
        // and want to give the user the impression of fast action.
        if (toaster.isShowing()) {
          toaster.hide();
        }

        // If the user wanted to add something else, proceed, since in many
        // cases, the user just had to wait a second or so before we could
        // proceed anyway.
        if (wantAttachment) {
          this.onAttachmentAdd();
        }
      }.bind(this), 600);
    },

    /**
     * Given a list of Blobs/Files that we want to attach, attach as many as
     * possible and generate an error message for any we can't attach.  This
     * will update the UI as a side-effect; you do not need to do it.
     */
    addAttachmentsSubjectToSizeLimits: function(toAttach, replaceAttach) {
      // Tally the size of the already-attached attachments.
      var totalSize = this.calculateTotalAttachmentsSize();

      // Keep attaching until we find one that puts us over the limit.  Then
      // generate an error whose plurality is based on the number of attachments
      // we are not attaching.  We do not do any bin-packing smarts where we try
      // and see if any of the attachments in `toAttach` might fit.
      //
      // This specific behaviour is potentially a little odd; we're going with
      // consistency of the original implementation of bug 871852 but without
      // the horrible bug introduced by bug 871897 and being addressed by this
      // in bug 1006271.
      var attachedAny = false;
      var replaceSize = 0;
      while (toAttach.length) {
        var attachment = toAttach.shift();
        if (replaceAttach) {
          replaceSize = replaceAttach.blob.size;
        }
        totalSize += attachment.blob.size;
        if (totalSize >= (MAX_ATTACHMENT_SIZE + replaceSize)) {
          this._warnAttachmentSizeExceeded(1 + toAttach.length);
          break;
        }

        this._totalAttachmentsFinishing += 1;
        if (replaceAttach) {
          this.composer.replaceAttachment(replaceAttach, attachment,
                                          this._onAttachmentDone);
        } else {
          this.composer.addAttachment(attachment, this._onAttachmentDone);
        }
        attachedAny = true;
      }

      if (attachedAny) {
        this.renderAttachments();
      }
    },

    /**
     * Build the UI that displays the current attachments.  Invokes
     * `updateAttachmentsSize` too so you don't have to.
     */
    renderAttachments: function() {
      if (this.composer.attachments && this.composer.attachments.length) {
        // Clean the container before we insert the new attachments
        this.attachmentsContainer.innerHTML = '';
        this.attachmentsName.innerHTML = '';

        var attTemplate = cmpAttachmentItemNode,
            filenameTemplate =
              attTemplate.getElementsByClassName('cmp-attachment-filename')[0],
            filesizeTemplate =
              attTemplate.getElementsByClassName('cmp-attachment-filesize')[0];

        for (var i = 0; i < this.composer.attachments.length; i++) {
          var attachment = this.composer.attachments[i];

          filenameTemplate.textContent = attachment.name;
          fileDisplay.fileSize(filesizeTemplate, attachment.blob.size);
          var attachmentNode = attTemplate.cloneNode(true);
          this.attachmentsContainer.appendChild(attachmentNode);
          this.attachmentsName.innerHTML += attachment.name + ' ';

          var mimeClass = mimeToClass(attachment.blob.type);
          attachmentNode.classList.add(mimeClass);
          var attachementIcon =
              attachmentNode.querySelector('.cmp-attachment-icon');
          var attachementDataIcon = '';
          switch (mimeClass) {
            case 'mime-audio':
              attachementDataIcon = 'file-audio';
              break;
            case 'mime-video':
              attachementDataIcon = 'file-video';
              break;
            case 'mime-image':
              attachementDataIcon = 'file-photo';
              break;
            case 'mime-zip':
              attachementDataIcon = 'file-compress';
              break;
            default:
              attachementDataIcon = 'file';
              break;
          }
          attachementIcon.setAttribute('data-icon', attachementDataIcon);
        }

        this.updateAttachmentsSize();
      }
      new Promise(function(resolve) {
        instance.updateAttachmentsAriaLabel();
        if (instance.type !== 'reply') {
          var index = NavigationMap.getCurrentControl().index;
          NavigationMap.setFocus(index);
        }
        resolve();
      }).then( () => {
        window.dispatchEvent(new CustomEvent('cmp-attachments-update', {
          detail: { composer: instance },
          bubbles: true,
          cancelable: false
        }));
      });
    },

    /**
     * Update the label used for accessibility that describes the attachments
     * included.
     */
    updateAttachmentsAriaLabel: function() {
      // Only include total size in KB if there is more than 1 attachment.
      var kilobytes = this.composer.attachments.length > 1 ?
        (this.calculateTotalAttachmentsSize() / 1024) : 0;
      var MB = parseFloat(kilobytes / 1024).toFixed(1);
      mozL10n.setAttributes(this.attachmentsContainer,
        'compose-attachments-container', { kilobytes: MB });
    },

    /**
     * Calculate the total size of all attachments included.
     */
    calculateTotalAttachmentsSize: function() {
      var totalSize = 0;
      for (var i = 0; i < this.composer.attachments.length; i++) {
        totalSize += this.composer.attachments[i].blob.size;
      }
      return totalSize;
    },

    /**
     * Update the summary that says how many attachments we have and the
     * aggregate attachment size.
     */
    updateAttachmentsSize: function() {
      mozL10n.setAttributes(this.attachmentLabel, 'compose-attachments',
                            { n: this.composer.attachments.length });

      if (this.composer.attachments.length === 0) {
        this.attachmentsSize.textContent = '';

        // When there is no attachments, hide the container
        // to keep the style of empty attachments
        this.attachmentsContainer.classList.add('collapsed');
      }
      else {
        fileDisplay.fileSize(this.attachmentsSize,
          this.calculateTotalAttachmentsSize());
      }

      // Only display the total size when the number of attachments is more than 1
      var needFocus = false;
      if (this.composer.attachments.length >= 1) {
        this.attachmentTotal.classList.remove('collapsed');
        this.attachmentTotal.classList.add('focusable');
        needFocus = true;
      } else {
        this.attachmentTotal.classList.add('collapsed');
        this.attachmentTotal.classList.remove('focusable');
      }
      NavigationMap.navSetup(this.localName, '.focusable');
      if (needFocus && this.getAttribute('aria-hidden') === 'false') {
        var elements = Array.prototype.slice.call
                       (NavigationMap.getCurrentControl().elements);
        NavigationMap.setFocus(elements.indexOf(this.attachmentTotal));
      }
    },

    onClickRemoveAttachment: function(attachIndex) {
      var node = this.attachmentsContainer.querySelector
                 ('[data-attachindex="' + attachIndex +'"]');
      node.parentNode.removeChild(node);
      var attachment = this.composer.attachments[attachIndex];
      this.composer.removeAttachment(attachment);
      toaster.toast({
         text: mozL10n.get('cmp-remove-attach')
      });
      // bug465-lijuanli@t2mobile.com-attachment display wrong after removing one-begin
      if(this.composer.attachments.length === 0) {
          new Promise(function(resolve) {
              instance.updateAttachmentsAriaLabel();
              instance.updateAttachmentsSize();
              resolve();
          }).then( () => {
              window.dispatchEvent(new CustomEvent('cmp-attachments-update', {
              detail: { composer: instance },
              bubbles: true,
              cancelable: false
            }));
          });
      } else {
        this.renderAttachments();
      }
      // bug465-lijuanli@t2mobile.com-attachment display wrong after removing one-end
    },

    /**
     * Save the draft if there's anything to it, close the card.
     */
    goBack: function() {
      if (this.activity) {
        // We need more testing here to make sure the behavior that back
        // to originated activity works perfectly without any crash or
        // unable to switch back.
        this.activity.postError('cancelled');
        this.activity = null;
        cards._backgroundUpdate = true;
      }
      if (NavigationMap.confirmShown) {
        NavigationMap.confirmShown = false;
      }
      this._closeCard();
    },

    onBack: function(callback) {
      if (!this._saveNeeded()) {
        console.log('compose: back: no save needed, exiting without prompt');
        if (callback) {
          callback();
        } else {
          this.goBack();
        }
        return;
      }

      if (!draftConfirmShown) {
        draftConfirmShown = true;
        NavigationMap.confirmShown = true;
        var dialogConfig = {
          title: {
            id: 'confirm-dialog-title',
            args: {}
          },
          body: {
            id: 'compose-draft-save',
            args: {}
          },
          desc: {
            id: '',
            args: {}
          },
          cancel: {
            l10nId: 'compose-draft-dialog-cancel',
            priority: 1,
            callback: function() {
              draftConfirmShown = false;
              NavigationMap.restoreFocus();
              cards._endKeyClicked = false;
            }
          },
          accept: {
            l10nId: 'compose-draft-dialog-save',
            priority: 2,
            callback: function() {
              instance._saveDraft('explicit');
              draftConfirmShown = false;
              NavigationMap.restoreFocus();
              cards._endKeyClicked = false;
              if (callback) {
                toaster.toast({
                  text: mozL10n.get('composer-draft-saved')
                });
                callback();
              } else {
                instance.goBack();
                toaster.toast({
                  text: mozL10n.get('composer-draft-saved')
                });
              }
            }
          },
          confirm: {
            l10nId: 'compose-discard-confirm',
            priority: 3,
            callback: function() {
              instance.composer.abortCompositionDeleteDraft();
              draftConfirmShown = false;
              NavigationMap.restoreFocus();
              cards._endKeyClicked = false;
              if (callback) {
                callback();
              } else {
                instance.goBack();
              }
            }
          }
        };

        var dialog = new ConfirmDialogHelper(dialogConfig);
        dialog.show(document.getElementById('confirm-dialog-container'));
      }
    },

    /**
     * Validate that the provided addresses are valid. Enable the send
     * button conditional on all addresses being correct. If all
     * addresses are correct and we had previously displayed a
     * sendStatus error, hide the sendStatus error display.
     *
     * @return {Boolean}
     *   True if all addresses are valid, otherwise false.
     */
    validateAddresses: function() {
      var addrs = this.extractAddresses();

      if (addrs.all.length === 0) {
        this.needSendKey = false;
      } else {
        if (addrs.all.length === addrs.invalid.length) {
          this.needSendKey = false;
        } else {
          this.needSendKey = true;
        }
      }
      // when first time enter the composer page, none softkeybar instance
      // has been created
      if (!doActive) {
        this.setSoftkey();
      }

      if (addrs.invalid.length === 0) {
        // If the error message is visible, meaning they opened this
        // message from the outbox after a send failure, remove the error
        // when they've corrected the recipients.
        this.errorMessage.classList.add('collapsed');
        return true; // No invalid addresses.
      } else {
        return false; // Some addresses were invalid.
      }
    },

    /**
     * If the user attempts to tap the send button while there are
     * invalid addresses, display a dialog to warn them to correct the
     * error. Otherwise, go ahead and send the message.
     */
    onSend: function() {
      if (!this.validateAddresses()) {
        document.activeElement.blur();
        var dialogConfig = {
          title: {
            id: 'compose-invalid-addresses-title',
            args: {}
          },
          body: {
            id: 'compose-invalid-addresses-description',
            args: {}
          },
          desc: {
            id: '',
            args: {}
          },
          accept: {
            l10nId: 'confirm-dialog-ok',
            priority: 2,
            callback: function() {
              var index = NavigationMap.getCurrentControl().index;
              NavigationMap.setFocus(index);
            }
          }
        };
        var dialog = new ConfirmDialogHelper(dialogConfig);
        dialog.show(document.getElementById('confirm-dialog-container'));
      } else {
        this.reallySend();
      }
    },

    /**
     * Actually send the message, foregoing any validation that the
     * addresses are valid (as we did in `onSend` above).
     */
    reallySend: function() {
      /* Check if already lock is enabled,
       * If so disable it and then re enable the lock
       */
      this._saveStateToComposer();

      var activity = this.activity;

      // Indicate we are sending so we can suppress any of our auto-save logic
      // from trying to fire.
      this.sending = true;

      // Initiate the send.
      console.log('compose: initiating send');
      evt.emit('uiDataOperationStart', this._dataIdSendEmail);

      this.composer.finishCompositionSendMessage(function(sendInfo) {
        evt.emit('uiDataOperationStop', this._dataIdSendEmail);

        // Card could have been destroyed in the meantime,
        // via an app card reset (not a _selfClosed case),
        // so do not bother with the rest of this work if
        // that was the case.
        if (!this.composer) {
          return;
        }

        if (activity) {
          // Just mention the action completed, but do not give
          // specifics, to maintain some privacy.
          activity.postResult('complete');
          activity = null;
          cards._backgroundUpdate = true;
        }

        this._closeCard();

      }.bind(this));
    },

    onccAdd: function() {
      this.cmp_cc.classList.remove('collapsed');
      this.cmp_cc.classList.add('focusable');
      NavigationMap.navSetup(this.localName, '.focusable');
      needRefresh = true;
      focusTo = this.cmp_cc;
      this.cmp_cc.scrollIntoView(true);
    },

    onccRemove: function() {
      var self = this;
      var ccList = this.cmp_cc.querySelectorAll('.cmp-peep-bubble');
      self.clearContacts(ccList);
      this.cmp_cc.classList.add('collapsed');
      this.cmp_cc.classList.remove('focusable');
      NavigationMap.navSetup(this.localName, '.focusable');
      needRefresh = true;
    },

    onbccAdd: function() {
      this.cmp_bcc.classList.remove('collapsed');
      this.cmp_bcc.classList.add('focusable');
      NavigationMap.navSetup(this.localName, '.focusable');
      needRefresh = true;
      focusTo = this.cmp_bcc;
      this.cmp_bcc.scrollIntoView(true);
    },

    onbccRemove: function() {
      var self = this;
      var bccList = this.cmp_bcc.querySelectorAll('.cmp-peep-bubble');
      self.clearContacts(bccList);
      this.cmp_bcc.classList.add('collapsed');
      this.cmp_bcc.classList.remove('focusable');
      NavigationMap.navSetup(this.localName, '.focusable');
      needRefresh = true;
    },

    refresh: function() {
      if (needRefresh) {
        this.updateSoftkey();
        needRefresh = false;
      }
      if (focusTo) {
        var elements = NavigationMap.getCurrentControl().elements;
        elements = Array.prototype.slice.call(elements);
        NavigationMap.setFocus(elements.indexOf(focusTo));
        focusTo = null;
      }
    },

    clearContacts: function(nodes) {
      var length = nodes.length;
      for (var i = 0; i < length; i++) {
        this.deleteBubble(nodes[i]);
      }
    },

    goAttachmentsList: function() {
      if (this.addATA.readyState === 'pending') {
        return;
      }
      console.log('go to attachments list');
      var attachments = document.querySelector('#cmp-attachmentContainer');
      cards.pushCard('cmp-attachments', 'animate', {
        composer: this
      });
    },

    onContactAdd: function() {
      if (this._selfClosed) {
        return;
      }
      var contactBtn = NavigationMap.getCurrentItem();
      var self = this;

      try {
        var activity = new MozActivity({
          name: 'pick',
          data: {
            type: 'webcontacts/email'
          }
        });
        activity.onsuccess = function success() {
          if (this.result.email) {
            var emt = contactBtn.querySelector('.cmp-addr-text');
            var name = this.result.name;
            if (Array.isArray(name)) {
              name = name[0];
            }
            self.insertBubble(emt, name, this.result.email);
          }
        };
        activity.onerror = function error() {
          console.log('Activities error');
          setTimeout( ()=> {
             NavigationMap.setFocus('restore');
          }, 300);
        }
      } catch (e) {
        console.log('WebActivities unavailable? : ' + e);
      }
    },

    onAttachmentAdd: function(event) {
      if (event) {
        event.stopPropagation();
      }

      // To be nice on memory consumption, wait for any previous attachment to
      // finish attaching before triggering another attachment action.
      if (this._totalAttachmentsFinishing > 0) {
        // Use a separate flag than testing if the toaster is showing, in case
        // the toaster is shown for some other reason. In that case, do not want
        // to trigger activity after previous attachment completes.
        this._wantAttachment = true;
        toaster.toast({
          text: mozL10n.get('compose-attachment-still-working')
        });
        return;
      }

      try {
        console.log('compose: attach: triggering web activity');
        this.addATA = new MozActivity({
          name: 'pick',
          data: {
            type: ['image/*', 'video/*', 'audio/*', 'application/*',
                   'text/vcard'],
            nocrop: true
          }
        });
        this.addATA.onsuccess = (function success() {
          // Load the util on demand, since one small codepath needs it, and
          // it avoids needing to bundle util's dependencies in a built layer.
          require(['attachment_name'], function(attachmentName) {
            var blob = this.addATA.result.blob,
                name = this.addATA.result.blob.name || this.addATA.result.name,
                count = this.composer.attachments.length + 1;

            name = attachmentName.ensureName(blob, name, count);
            var filename = name.substring(name.lastIndexOf('/') + 1);

            this.addAttachmentsSubjectToSizeLimits([{
              name: filename,
              pathName: name,
              blob: blob
            }]);
          }.bind(this));
        }).bind(this);
      } catch (e) {
        console.log('WebActivities unavailable? : ' + e);
      }
    },

    onAttachmentReplace: function(){
      var replacedIndex = document.querySelector('.hasfocused').
                          getAttribute('data-attachIndex');
      if (!replacedIndex) {
        console.log('Error index of attachment');
        return;
      }
      var replaceAttach = this.composer.attachments[replacedIndex];
      // To be nice on memory consumption, wait for any previous attachment to
      // finish attaching before triggering another attachment action.
      if (this._totalAttachmentsFinishing > 0) {
        // Use a separate flag than testing if the toaster is showing, in case
        // the toaster is shown for some other reason. In that case, do not want
        // to trigger activity after previous attachment completes.
        this._wantAttachment = true;
        toaster.toast({
          text: mozL10n.get('compose-attachment-still-working')
        });
        return;
      }

      try {
        console.log('compose: attach: triggering web activity');
        var activity = new MozActivity({
          name: 'pick',
          data: {
            type: ['image/*', 'video/*', 'audio/*', 'application/*',
                   'text/vcard'],
            nocrop: true
          }
        });
        activity.onsuccess = (function success() {
          // Load the util on demand, since one small codepath needs it, and
          // it avoids needing to bundle util's dependencies in a built layer.
          require(['attachment_name'], function(attachmentName) {
            var blob = activity.result.blob,
                name = activity.result.blob.name || activity.result.name,
                count = this.composer.attachments.length + 1;

            name = attachmentName.ensureName(blob, name, count);

            console.log('compose: attach activity success:', name);

            name = name.substring(name.lastIndexOf('/') + 1);

            this.addAttachmentsSubjectToSizeLimits([{
              name: name,
              blob: activity.result.blob
            }],replaceAttach);
          }.bind(this));
        }).bind(this);
      } catch (e) {
        console.log('WebActivities unavailable? : ' + e);
      }
    },

    onHidden: function() {
      window.removeEventListener('keydown', this.keydownHandler);
      window.removeEventListener('email-endkey', this.endkeyHandler);
    },

    die: function() {
      // If confirming for prompt when destroyed, just remove
      // and if save is needed, it will be autosaved below.
      if (this._savePromptMenu) {
        document.body.removeChild(this._savePromptMenu);
        this._savePromptMenu = null;
      }

      // If something else besides the card causes this card
      // to die, but we have a draft to save, do it now.
      // However, wait for the draft save to complete before
      // completely shutting down the composer.
      if (!this._selfClosed && this._saveNeeded()) {
        console.log('compose: autosaving draft because not self-closed');
        this._saveDraft('automatic');
      }

      if (this.composer) {
        this.composer.die();
        this.composer = null;
      }
      window.removeEventListener('menuEvent', this.menuHandler);
      window.removeEventListener('keydown', this.keydownHandler);
      window.removeEventListener('email-endkey', this.endkeyHandler);
    },

    searchContacts: function(evt) {
      var target = evt.target;
      var typed = evt.target.value;

      if (typed) {
        Contacts.findContactByString(typed, target,
            this.renderSuggestions.bind(this));
      } else {
        this.clearSuggestionList();
        this.stopNavOnSuggestionList();
        this.setSoftkey();
      }
    },

    renderSuggestions: function(contacts, inputValue, insertNode) {
      if (contacts.length === 0) {
        this.clearSuggestionList();
        this.stopNavOnSuggestionList();
        return;
      }

      if (document.activeElement !== insertNode) {
        return;
      }

      var itemIndex = 0;
      var tTop = this.getElementsByClassName('cmp-addr-bar')[0].offsetTop;
      var pHeight = insertNode.parentNode.parentNode.parentNode.clientHeight;
      var top = pHeight + tTop;
      var suggestionsListHeight = this.scrollContainer.clientHeight - pHeight;
      this.suggestionsList.innerHTML = '';
      this.suggestionsContainer.style.setProperty('top', top + 'px');
      this.suggestionsContainer.style.setProperty('max-height',
          suggestionsListHeight + 'px');
      this.suggestionsContainer.classList.add('show');

      suggestionList = true;

      contacts.forEach(function(contact) {
        var name = contact.name[0];
        var emails = contact.email;
        if (!emails || emails.length === 0) {
          return true;
        }
        emails.forEach(function(item) {
          var ismatch = instance.isMatch(inputValue.terms, name, item.value);
          if (ismatch) {
            itemIndex++;
            var node = suggestionItem.cloneNode(true);
            var content =
                node.innerHTML.replace(new RegExp('{{name}}', 'gm'), name)
                    .replace(new RegExp('{{type}}', 'gm'), item.type[0])
                    .replace(new RegExp('{{email}}', 'gm'), item.value);
            node.innerHTML = content;
            instance.highlightMatch(inputValue.terms, node);
            instance.suggestionsList.appendChild(node);
            node.addEventListener('click', function() {
              var index = NavigationMap.getCurrentControl().index;
              instance.clearSuggestionList();
              instance.insertBubble(insertNode, name, item.value);
              insertNode.value = '';
              NavigationMap.setFocus(index);
            });
          }
        });
      });

      if (this.suggestionsList.children.length === 0) {
        this.clearSuggestionList();
        this.stopNavOnSuggestionList();
        return;
      }

      this.suggestionsContainer.scrollTop = 0;
      if (insertNode.classList.contains('cmp-cc-text')) {
        this.cmp_cc.scrollIntoView(true);
      } else if (insertNode.classList.contains('cmp-bcc-text')) {
        this.cmp_bcc.scrollIntoView(true);
      }
      if (!suggestionListNavListener) {
        this.startNavOnSuggestionList();
      }
    },

    highlightMatch: function(text, node) {
      var name = node.querySelector('.ellipsis-dir-fix');
      var email = node.querySelector('.email-detail');

      var input = text.join(' ');
      var reg = new RegExp(input, 'mi');

      if (reg.test(name.innerHTML)) {
        name.innerHTML = name.innerHTML.split(input)
            .join('<span>' + input + '</span>');
      }
      if (reg.test(email.innerHTML)) {
        email.innerHTML = email.innerHTML.split(input)
            .join('<span>' + input + '</span>');
      }
    },

    clearSuggestionList: function(node) {
      if (node) {
        var evt = new CustomEvent('backspace-long-press');
        window.dispatchEvent(evt);
      }
      this.suggestionsContainer.classList.remove('show');
      this.suggestionsList.innerHTML = '';
      suggestionList = false;
      if (node && !this._selfClosed) {
        this.searchContacts( {target: node} );
      }
    },

    isMatch: function(inputValue, name, email)  {
      var input = inputValue.join(' ');
      var reg = new RegExp(input, 'mi');
      return reg.test(name) || reg.test(email);
    },

    hideDialog: function() {
      draftConfirmShown = false;
      CustomDialog.hide();
    },

    onBodyNodeKeydown: function(evt) {
      var range = window.getSelection().getRangeAt(0);
      var currentElement = range.startContainer;
      switch (evt.key) {
        case 'ArrowUp':
          if ((currentElement === document.activeElement ||
              currentElement === document.activeElement.firstChild)
              && range.startOffset === 0) {
            break;
          }
          evt.stopPropagation();
          break;
        case 'ArrowDown':
          if ((currentElement === document.activeElement && isLastLine(range))
              || isLastNode(range)) {
            break;
          }
          evt.stopPropagation();
          break;
      }

      function isLastLine(range) {
        if (document.activeElement.lastChild.tagName === 'BR' &&
            range.startOffset === (document.activeElement.childNodes.length - 1)
        ) {
          return true;
        } else if (range.startOffset ===
            document.activeElement.childNodes.length) {
          return true;
        }
        return false;
      }

      function isLastNode(range) {
        if (range.startOffset === range.startContainer.length) {
          return true;
        }
        return false;
      }
    }
  }
];

});
