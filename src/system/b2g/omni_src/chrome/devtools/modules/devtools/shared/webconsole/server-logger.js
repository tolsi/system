"use strict";const{Cu,Ci}=require("chrome");const{Class}=require("sdk/core/heritage");const Services=require("Services");const{DebuggerServer}=require("devtools/server/main");const DevToolsUtils=require("devtools/shared/DevToolsUtils");loader.lazyGetter(this,"NetworkHelper",()=>require("devtools/shared/webconsole/network-helper"));const trace={log:function(){}};const makeInfallible=DevToolsUtils.makeInfallible;const acceptableHeaders=["x-chromelogger-data"];var ServerLoggingListener=Class({initialize:function(win,owner){trace.log("ServerLoggingListener.initialize; ",owner.actorID,", child process: ",DebuggerServer.isInChildProcess);this.owner=owner;this.window=win;this.onExamineResponse=this.onExamineResponse.bind(this);this.onExamineHeaders=this.onExamineHeaders.bind(this);this.onParentMessage=this.onParentMessage.bind(this);this.attach();},destroy:function(){trace.log("ServerLoggingListener.destroy; ",this.owner.actorID,", child process: ",DebuggerServer.isInChildProcess);this.detach();},attach:makeInfallible(function(){trace.log("ServerLoggingListener.attach; child process: ",DebuggerServer.isInChildProcess);


if(DebuggerServer.isInChildProcess){this.attachParentProcess();}else{Services.obs.addObserver(this.onExamineResponse,"http-on-examine-response",false);}}),detach:makeInfallible(function(){trace.log("ServerLoggingListener.detach; ",this.owner.actorID);if(DebuggerServer.isInChildProcess){this.detachParentProcess();}else{Services.obs.removeObserver(this.onExamineResponse,"http-on-examine-response",false);}}), attachParentProcess:function(){trace.log("ServerLoggingListener.attachParentProcess;");this.owner.conn.setupInParent({module:"devtools/shared/webconsole/server-logger-monitor",setupParent:"setupParentProcess"});let mm=this.owner.conn.parentMessageManager;let{addMessageListener,sendSyncMessage}=mm;


addMessageListener("debug:server-logger",this.onParentMessage);sendSyncMessage("debug:server-logger",{method:"attachChild"});},detachParentProcess:makeInfallible(function(){trace.log("ServerLoggingListener.detachParentProcess;");let mm=this.owner.conn.parentMessageManager;let{removeMessageListener,sendSyncMessage}=mm;sendSyncMessage("debug:server-logger",{method:"detachChild",});removeMessageListener("debug:server-logger",this.onParentMessage);}),onParentMessage:makeInfallible(function(msg){if(!msg.data){return;}
let method=msg.data.method;trace.log("ServerLogger.onParentMessage; ",method,msg.data);switch(method){case"examineHeaders":this.onExamineHeaders(msg);break;default:trace.log("Unknown method name: ",method);}}), onExamineHeaders:function(event){let headers=event.data.headers;trace.log("ServerLoggingListener.onExamineHeaders;",headers);let parsedMessages=[];for(let item of headers){let header=item.header;let value=item.value;let messages=this.parse(header,value);if(messages){parsedMessages.push(...messages);}}
if(!parsedMessages.length){return;}
for(let message of parsedMessages){this.sendMessage(message);}},onExamineResponse:makeInfallible(function(subject){let httpChannel=subject.QueryInterface(Ci.nsIHttpChannel);trace.log("ServerLoggingListener.onExamineResponse; ",httpChannel.name,", ",this.owner.actorID,httpChannel);if(!this._matchRequest(httpChannel)){trace.log("ServerLoggerMonitor.onExamineResponse; No matching request!");return;}
let headers=[];httpChannel.visitResponseHeaders((header,value)=>{header=header.toLowerCase();if(acceptableHeaders.indexOf(header)!==-1){headers.push({header:header,value:value});}});this.onExamineHeaders({data:{headers:headers,}});}),_matchRequest:function(channel){trace.log("_matchRequest ",this.window,", ",this.topFrame);if(!this.window){return true;}

if(!channel.loadInfo&&channel.loadInfo.loadingDocument===null&&channel.loadInfo.loadingPrincipal===Services.scriptSecurityManager.getSystemPrincipal()){return false;}

let win=NetworkHelper.getWindowForRequest(channel);while(win){if(win==this.window){return true;}
if(win.parent==win){break;}
win=win.parent;}
return false;}, parse:function(header,value){let data;try{let result=decodeURIComponent(escape(atob(value)));data=JSON.parse(result);}catch(err){Cu.reportError("Failed to parse HTTP log data! "+err);return null;}
let parsedMessage=[];let columnMap=this.getColumnMap(data);trace.log("ServerLoggingListener.parse; ColumnMap",columnMap);trace.log("ServerLoggingListener.parse; data",data);let lastLocation;for(let row of data.rows){let backtrace=row[columnMap.get("backtrace")];let rawLogs=row[columnMap.get("log")];let type=row[columnMap.get("type")]||"log";if(data.columns.indexOf("label")!=-1){let label=row[columnMap.get("label")];let showLabel=label&&typeof label==="string";rawLogs=[rawLogs];if(showLabel){rawLogs.unshift(label);}}


let location=this.parseBacktrace(backtrace);if(location){lastLocation=location;}else{location=lastLocation;}
parsedMessage.push({logs:rawLogs,location:location,type:type});}
return parsedMessage;},parseBacktrace:function(backtrace){if(!backtrace){return null;}
let result=backtrace.match(/\s*(\d+)$/);if(!result||result.length<2){return backtrace;}
return{url:backtrace.slice(0,-result[0].length),line:result[1]};},getColumnMap:function(data){let columnMap=new Map();let columnName;for(let key in data.columns){columnName=data.columns[key];columnMap.set(columnName,key);}
return columnMap;},sendMessage:function(msg){trace.log("ServerLoggingListener.sendMessage; message",msg);let formatted=format(msg);trace.log("ServerLoggingListener.sendMessage; formatted",formatted);let win=this.window;let innerID=win?getInnerId(win):null;let location=msg.location;let message={category:"server",innerID:innerID,level:msg.type,filename:location?location.url:null,lineNumber:location?location.line:null,columnNumber:0,private:false,timeStamp:Date.now(),arguments:formatted?formatted.logs:null,styles:formatted?formatted.styles:null,};if(msg.type=="group"&&formatted&&formatted.logs){message.groupName=formatted?formatted.logs[0]:null;}


let args=message.arguments;if(msg.type=="table"&&args){if(typeof args[0]=="string"){args.shift();}}
trace.log("ServerLoggingListener.sendMessage; raw: ",msg.logs.join(", "),message);this.owner.onServerLogCall(message);},}); function format(msg){if(!msg.logs||!msg.logs[0]){return null;}
msg.styles=[];let firstString="";if(typeof msg.logs[0]=="string"){firstString=msg.logs.shift();}
let splitLogRegExp=/(.*?)(%[oOcsdif]|$)/g;let splitLogRegExpRes;let concatString="";let pushConcatString=()=>{if(concatString){rebuiltLogArray.push(concatString);}
concatString="";};
let rebuiltLogArray=[];
while((splitLogRegExpRes=splitLogRegExp.exec(firstString))!==null){let[,log,specifier]=splitLogRegExpRes;
if(log){concatString+=log;}

if(!specifier){break;}
let argument=msg.logs.shift();switch(specifier){case"%i":case"%d":concatString+=(argument|0);break;case"%f":concatString+=(+argument);break;case"%o":case"%O":pushConcatString();rebuiltLogArray.push(argument);break;case"%s":concatString+=argument;break;case"%c":pushConcatString();let fillNullArrayLength=rebuiltLogArray.length-msg.styles.length;let fillNullArray=Array(fillNullArrayLength).fill(null);msg.styles.push(...fillNullArray,argument);break;}}
if(concatString){rebuiltLogArray.push(concatString);}

msg.logs.unshift(...rebuiltLogArray);

for(let log of msg.logs){if(typeof log=="object"){delete log.___class_name;}}
return msg;}

function getInnerId(win){return win.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindowUtils).currentInnerWindowID;}
exports.ServerLoggingListener=ServerLoggingListener;