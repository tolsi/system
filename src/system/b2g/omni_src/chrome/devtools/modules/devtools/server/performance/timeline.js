"use strict";const{Ci,Cu}=require("chrome");const{Class}=require("sdk/core/heritage");
loader.lazyRequireGetter(this,"events","sdk/event/core");loader.lazyRequireGetter(this,"Timers","sdk/timers");loader.lazyRequireGetter(this,"Task","resource://gre/modules/Task.jsm",true);loader.lazyRequireGetter(this,"Memory","devtools/server/performance/memory",true);loader.lazyRequireGetter(this,"Framerate","devtools/server/performance/framerate",true);loader.lazyRequireGetter(this,"StackFrameCache","devtools/server/actors/utils/stack",true);loader.lazyRequireGetter(this,"EventTarget","sdk/event/target",true);

const DEFAULT_TIMELINE_DATA_PULL_TIMEOUT=200; var Timeline=exports.Timeline=Class({extends:EventTarget,initialize:function(tabActor){this.tabActor=tabActor;this._isRecording=false;this._stackFrames=null;this._memory=null;this._framerate=null; this._onWindowReady=this._onWindowReady.bind(this);this._onGarbageCollection=this._onGarbageCollection.bind(this);events.on(this.tabActor,"window-ready",this._onWindowReady);},destroy:function(){this.stop();events.off(this.tabActor,"window-ready",this._onWindowReady);this.tabActor=null;},get docShells(){let originalDocShell;let docShells=[];if(this.tabActor.isRootActor){originalDocShell=this.tabActor.docShell;}else{originalDocShell=this.tabActor.originalDocShell;}
if(!originalDocShell){return docShells;}
let docShellsEnum=originalDocShell.getDocShellEnumerator(Ci.nsIDocShellTreeItem.typeAll,Ci.nsIDocShell.ENUMERATE_FORWARDS);while(docShellsEnum.hasMoreElements()){let docShell=docShellsEnum.getNext();docShells.push(docShell.QueryInterface(Ci.nsIDocShell));}
return docShells;},_pullTimelineData:function(){let docShells=this.docShells;if(!this._isRecording||!docShells.length){return;}
let endTime=docShells[0].now();let markers=[];if(this._withMarkers||this._withDocLoadingEvents){for(let docShell of docShells){for(let marker of docShell.popProfileTimelineMarkers()){markers.push(marker);


if(this._withFrames){if(marker.stack){marker.stack=this._stackFrames.addFrame(Cu.waiveXrays(marker.stack));}
if(marker.endStack){marker.endStack=this._stackFrames.addFrame(Cu.waiveXrays(marker.endStack));}}
if(this._withDocLoadingEvents){if(marker.name=="document::DOMContentLoaded"||marker.name=="document::Load"){events.emit(this,"doc-loading",marker,endTime);}}}}}
if(this._withMarkers&&markers.length>0){events.emit(this,"markers",markers,endTime);}
if(this._withTicks){events.emit(this,"ticks",endTime,this._framerate.getPendingTicks());}
if(this._withMemory){events.emit(this,"memory",endTime,this._memory.measure());}
if(this._withFrames&&this._withMarkers){let frames=this._stackFrames.makeEvent();if(frames){events.emit(this,"frames",endTime,frames);}}
this._dataPullTimeout=Timers.setTimeout(()=>{this._pullTimelineData();},DEFAULT_TIMELINE_DATA_PULL_TIMEOUT);},isRecording:function(){return this._isRecording;},start:Task.async(function*({withMarkers,withTicks,withMemory,withFrames,withGCEvents,withDocLoadingEvents,}){let docShells=this.docShells;if(!docShells.length){return-1;}
let startTime=this._startTime=docShells[0].now();if(this._isRecording){return startTime;}
this._isRecording=true;this._withMarkers=!!withMarkers;this._withTicks=!!withTicks;this._withMemory=!!withMemory;this._withFrames=!!withFrames;this._withGCEvents=!!withGCEvents;this._withDocLoadingEvents=!!withDocLoadingEvents;if(this._withMarkers||this._withDocLoadingEvents){for(let docShell of docShells){docShell.recordProfileTimelineMarkers=true;}}
if(this._withTicks){this._framerate=new Framerate(this.tabActor);this._framerate.startRecording();}
if(this._withMemory||this._withGCEvents){this._memory=new Memory(this.tabActor,this._stackFrames);this._memory.attach();}
if(this._withGCEvents){events.on(this._memory,"garbage-collection",this._onGarbageCollection);}
if(this._withFrames&&this._withMarkers){this._stackFrames=new StackFrameCache();this._stackFrames.initFrames();}
this._pullTimelineData();return startTime;}),stop:Task.async(function*(){let docShells=this.docShells;if(!docShells.length){return-1;}
let endTime=this._startTime=docShells[0].now();if(!this._isRecording){return endTime;}
if(this._withMarkers||this._withDocLoadingEvents){for(let docShell of docShells){docShell.recordProfileTimelineMarkers=false;}}
if(this._withTicks){this._framerate.stopRecording();this._framerate.destroy();this._framerate=null;}
if(this._withMemory||this._withGCEvents){this._memory.detach();this._memory.destroy();}
if(this._withGCEvents){events.off(this._memory,"garbage-collection",this._onGarbageCollection);}
if(this._withFrames&&this._withMarkers){this._stackFrames=null;}
this._isRecording=false;this._withMarkers=false;this._withTicks=false;this._withMemory=false;this._withFrames=false;this._withDocLoadingEvents=false;this._withGCEvents=false;Timers.clearTimeout(this._dataPullTimeout);return endTime;}),_onWindowReady:function({window}){if(this._isRecording){let docShell=window.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIWebNavigation).QueryInterface(Ci.nsIDocShell);docShell.recordProfileTimelineMarkers=true;}},_onGarbageCollection:function({collections,gcCycleNumber,reason,nonincrementalReason}){let docShells=this.docShells;if(!this._isRecording||!docShells.length){return;}
let endTime=docShells[0].now();events.emit(this,"markers",collections.map(({startTimestamp:start,endTimestamp:end})=>{return{name:"GarbageCollection",causeName:reason,nonincrementalReason:nonincrementalReason,cycle:gcCycleNumber,start,end,};}),endTime);},});